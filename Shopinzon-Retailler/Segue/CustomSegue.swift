//
//  CustomSegue.swift
//  Shopinzon
//
//  Created by Macho Uzan on 15/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit

class PushWithAnimation: UIStoryboardSegue {
    override func perform() {
        // Assign the source and destination views to local variables.
        //Insert the destination view above the current (source) one.
        if let firstVCView = self.source.view,
            let secondVCView = self.destination.view {
            // Specify the initial position of the destination view.
            secondVCView.frame = CGRect(x:screenWidth,y: 0.0,width: screenWidth,height: screenHeight)
            appDelegate.window?.insertSubview(secondVCView, aboveSubview: firstVCView)
            
            // Animate the transition.
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                firstVCView.frame = CGRect(x:-screenWidth,y: 0.0,width: firstVCView.frame.width,height: firstVCView.frame.height)
                secondVCView.frame = CGRect(x:0,y: 0.0,width: secondVCView.frame.width,height: secondVCView.frame.height)
            }) { (Finished) -> Void in
                self.source.present(self.destination as UIViewController, animated: false, completion: nil)
            }
        }
        
    }
}
class Push: UIStoryboardSegue {
    override func perform() {
        // Assign the source and destination views to local variables.
        if let firstVCView = self.source.view,
            let secondVCView = self.destination.view {
            // Specify the initial position of the destination view.
            secondVCView.frame = CGRect(x:screenWidth,y: 0.0,width: screenWidth,height: screenHeight)
            //Insert the destination view above the current (source) one.
            appDelegate.window?.insertSubview(secondVCView, aboveSubview: firstVCView)
            
            // Animate the transition.
            DispatchQueue.main.async {
                firstVCView.frame = CGRect(x:-screenWidth,y: 0.0,width: firstVCView.frame.width,height: firstVCView.frame.height)
                secondVCView.frame = CGRect(x:0,y: 0.0,width: secondVCView.frame.width,height: secondVCView.frame.height)
                self.source.present(self.destination as UIViewController, animated: false, completion: nil)
            }
            
        }
        
    }
}
