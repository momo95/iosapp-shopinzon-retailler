//
//  FloatExtension.swift
//  Shopinzon
//
//  Created by Macho Uzan on 05/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation

extension Float {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
