//
//  ChatDealHeader.swift
//  Shopinzon
//
//  Created by Macho Uzan on 18/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase
class ChatContextHeader:UICollectionReusableView {
    //MARK: Outlet
    @IBOutlet weak var product_title: UILabel!
    @IBOutlet weak var product_onlinePrice: UILabel!
    @IBOutlet weak var product_shopPrice: UILabel!
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var onlinePrice_view: UIView!
    
}

