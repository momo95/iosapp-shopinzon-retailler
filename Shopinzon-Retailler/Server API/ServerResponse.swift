//
//  ServerResponse.swift
//  Shopinzon
//
//  Created by Macho Uzan on 04/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
enum ServerStatus:Int {
    case success = 26
    case nonValidRequest = 100
    case databaseError = 200
    case invalidToken = 300
    case invalidEmail = 302
    case onlyForConnectedUser = 303
    case error = 500
}
class ServerResponse {
    var status:ServerStatus
    var content:JSON?
    init?(json:JSON){
        if let statusValue = json["status"].int,
            let status = ServerStatus(rawValue: statusValue) {
            self.status = status
            if json["content"].exists() {
                self.content = json["content"]
            }
        }
        else {
            return nil
        }
    }
}

