//
//  Testing.swift
//  Shopinzon
//
//  Created by Macho Uzan on 19/04/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import Firebase
import CoreLocation

let server_request_queue = DispatchQueue(label: "server_request_queue.response", qos: .utility, attributes: [.concurrent])
//                print(String(data: (response.request?.httpBody)!, encoding: .utf8))

class Server {
    //MARK: Properties
    static var _locationShops:(location:Location,shops:JSON)? = nil
    static var locationShops:(location:Location,shops:JSON)? {
        get {
            if _locationShops == nil {
                if let locationData = UserDefaults.standard.object(forKey: "location") as? NSData,
                    let shopsData = UserDefaults.standard.object(forKey: "shops") as? NSData {
                    if let location = NSKeyedUnarchiver.unarchiveObject(with: locationData as Data) as? Location ,
                        let shops = NSKeyedUnarchiver.unarchiveObject(with: shopsData as Data) as? String {
                        self._locationShops = (location,JSON(shops.data(using: String.Encoding.utf8, allowLossyConversion: false)! ))
                    }
                }
            }
            return _locationShops
        }
        set(newValue){
            if let newValue = newValue {
                let location = NSKeyedArchiver.archivedData(withRootObject: newValue.location)
                let shops = NSKeyedArchiver.archivedData(withRootObject: newValue.shops.rawString(String.Encoding.utf8)!)
                UserDefaults.standard.set(location, forKey: "location")
                UserDefaults.standard.set(shops, forKey: "shops")
            }
            self._locationShops = newValue
            
        }
    }
    static var _fcmToken:String? = nil
    static var fcmToken:String {
        get {
            if _fcmToken == nil {
                if let fcmTokenData = UserDefaults.standard.object(forKey: "fcmToken") as? NSData {
                    if let fcmToken = NSKeyedUnarchiver.unarchiveObject(with: fcmTokenData as Data) as? String {
                        self._fcmToken = fcmToken
                        return _fcmToken!
                    }
                }
                return "fake_fcm"
            }
            return _fcmToken!
        }
        set(newValue){
            let fcmToken = NSKeyedArchiver.archivedData(withRootObject: newValue)
            UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
            self._fcmToken = newValue
        }
    }
    
    static func authentification_headers(completion:@escaping (_ authentification_token:HTTPHeaders) -> Void)  {
        let startTime = CFAbsoluteTimeGetCurrent()
        
        if let user = Auth.auth().currentUser{
            user.getIDToken{ idToken, error in
                if error != nil {
                    // Handle error
                    return;
                }
                //print(idToken)
                //print("Time elapsed for authentification_headers: \(CFAbsoluteTimeGetCurrent() - startTime) s.")
                completion( ["shopinzon-token": idToken!])
            }
        }
    }
    static func logout(){
        do{
            try Auth.auth().signOut()
        }
        catch{
            fatalError("The Fire not sign out.")
        }
    }
    
    static func login(email:String,password:String,completion:@escaping (_ custom_token:String?) -> Void){
        let url = domain + "/login/"
        let params = ["email": email,"password": password] as [String : Any]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON(queue: server_request_queue) { response in
            print(response)
            if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                if let custom_token = serverResponse.content?["custom_token"].stringValue{
                    completion(custom_token)
                }
            }
        }
    }
    static func me(threadQueue:DispatchQueue = server_request_queue,completion:@escaping (_ shop:Shop,_ catalog:[Product]) -> Void){
        let url = domain + "/me/"
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, headers: authentification_headers).responseJSON(queue: threadQueue) {
                response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if let content:JSON = serverResponse.content{
                        let shop = Shop(json:content["shop"])
                        var products:[Product] = []
                        if let catalogJson:JSON = content["catalog"] {
                            for productJson in catalogJson.arrayValue {
                                let product = Product(json:productJson["product"])!
                                product.shopPrice = productJson["prodshop"]["price"].float
                                products.append(product)
                            }
                        }
                        completion(shop!, products)
                    }
                }
            }
        }
    }
    static func getClient(user_id:String,threadQueue:DispatchQueue = server_request_queue,completion: @escaping (_ user: User?) -> Void) {
        let url = domain + "/client/"
        let params = ["id": user_id]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) {
                response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    print(response.result.value)
                    if let body = response.request?.httpBody {
                        print(String(data: body, encoding: .utf8))
                    }
                    if let userJson:JSON = serverResponse.content{
                        let user = User(json: userJson["user"])!
                        completion(user)
                    }
                }
            }
        }
    }
    static func getChatList(user_ids:[String],threadQueue:DispatchQueue = server_request_queue,completion: @escaping (_ user: [User]) -> Void) {
        let join = user_ids.joined(separator: "%22%2C%22")
        let url = domain + "/client/chat/list?ids=[%22\(join)%22]"
        print(url)
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, headers: authentification_headers).responseJSON(queue: threadQueue) {
                response in
                print(response.result.value)
                if let body = response.request?.httpBody {
                    print(String(data: body, encoding: .utf8))
                }
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if let usersJson:JSON = serverResponse.content{
                        if let users = usersJson.array?.map({User(json: $0)}).filter({$0 != nil}) {
                            completion(users as! [User])
                        }
                        else {
                            completion([])
                        }
                    }
                }
            }
        }
    }
    static func getProduct(product_id:UInt64,threadQueue:DispatchQueue = server_request_queue,completion: @escaping (_ product: Product?) -> Void) {
        let url = domain + "/product/"
        let params = ["id": product_id]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if let productJson:JSON = serverResponse.content{
                        let product = Product(json: productJson["product"])
                        product?.shopPrice = productJson["prodshop"]["price"].float
                        completion(product)
                    }
                }
            }
        }
    }
    static func sendMessage(recipient:String,message:Message,threadQueue:DispatchQueue = server_request_queue,completion:@escaping (_ done: ServerStatus) -> Void) {
        let url = domain + "/chat/send"
        let params = ["recipient": recipient,"message": message.json] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .put, parameters: params,encoding: JSONEncoding.default, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    print(response.result.value)
                    completion(serverResponse.status)
                }
            }
        }
    }
    static func onReadMessage(interlocutor:String,threadQueue:DispatchQueue = server_request_queue,completion:@escaping (_ done: ServerStatus) -> Void) {
        let url = domain + "/chat/read"
        let params = ["interlocutor": interlocutor] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    print(response.result.value)
                    completion(serverResponse.status)
                }
            }
        }
    }
    static func refreshPushToken(new:String,threadQueue:DispatchQueue = server_request_queue,completion:@escaping (_ done: ServerStatus) -> Void) {
        let url = domain + "/refresh-push-token/"
        let params = ["old": fcmToken,"aNew": new] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                print(response.result.value)
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if serverResponse.status == .success {
                        fcmToken = new
                    }
                    completion(serverResponse.status)
                }
            }
        }
    }
    static func getOrderList(from:UInt64 = UInt64(Date().timeIntervalSince1970)*1000,
                             threadQueue:DispatchQueue = server_request_queue,
                             completion:@escaping (_ ordersViews: [(order:Order,product:Product,user:Order.User)]) -> Void) {
        let url = domain + "/sale/list"
        let params = ["from":from] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                print(response)
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if let listOfOrdersJson:JSON = serverResponse.content {
                        print(listOfOrdersJson)
                        var orders:[(order:Order,product:Product,user:Order.User)] = []
                        for orderJson in listOfOrdersJson.arrayValue {
                            if let order = Order(json: orderJson["sale"]),
                                let product = Product(json: orderJson["product"])
                            {
                                let user = order.user
                                orders.append((order,product,user))
                            }
                        }
                        completion(orders)
                    }
                }
            }
        }
    }
    static func getOrderListForUser(userId:String,from:UInt64 = UInt64(Date().timeIntervalSince1970)*1000,
                             threadQueue:DispatchQueue = server_request_queue,
                             completion:@escaping (_ ordersViews: [(order:Order,product:Product,user:Order.User)]) -> Void) {
        let url = domain + "/sale/user/list"
        let params = ["userId":userId,"from":from] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                print(response)
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if let listOfOrdersJson:JSON = serverResponse.content {
                        print(listOfOrdersJson)
                        var orders:[(order:Order,product:Product,user:Order.User)] = []
                        for orderJson in listOfOrdersJson.arrayValue {
                            if let order = Order(json: orderJson["sale"]),
                                let product = Product(json: orderJson["product"])
                            {
                                let user = order.user
                                orders.append((order,product,user))
                            }
                        }
                        completion(orders)
                    }
                }
            }
        }
    }
    static func getClientList(from:String? = nil,
                              query:String? = nil,
                              levels:[Int]? = nil,
                              threadQueue:DispatchQueue = server_request_queue,
                              completion:@escaping (_ clientView: [Views.Client]) -> Void) {
        let url = domain + "/client/list"
        var params:[String : Any] = [:]
        if let from = from {
            params["from"] = from
        }
        if let query = query {
            params["query"] = query
        }
        if let levels = levels{
            params["levels"] = levels.urlObjectList()
        }
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                print(response.request?.url?.absoluteString)
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    if let clientListJson = serverResponse.content?.array{
                        completion(clientListJson.map({ Views.Client(json : $0)! }))
                    }
                }
            }
        }
    }
    static func getClient(userId:String,
                              threadQueue:DispatchQueue = server_request_queue,
                              completion:@escaping (_ clientView: Views.Client) -> Void) {
        let url = domain + "/client/"
        var params:[String : Any] = ["id":userId]
        
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .get, parameters: params, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                print(response)
                if let serverResponse = ServerResponse(json: JSON(response.result.value)),
                    serverResponse.status == .success,
                    let content = serverResponse.content{
                    if let clientView = Views.Client(json : content) {
                        completion(clientView)
                    }
                }
            }
        }
    }
    static func commentAClient(userId:String,
                               comment:Comment,
                               threadQueue:DispatchQueue = server_request_queue,
                               completion:@escaping (_ commented: ServerStatus) -> Void){
        let url = domain + "/client/comment"
        let params = [ "userId": userId,"comment" : comment.json] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .post, parameters: params,encoding: JSONEncoding.default, headers: authentification_headers).responseJSON(queue: threadQueue) { response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    completion(serverResponse.status)
                }
            }
        }
    }
    static func addPointToClient(userId:String,
                                 points:Int,
                                 threadQueue:DispatchQueue = server_request_queue,
                                 completion:@escaping (_ commented: ServerStatus) -> Void){
        let url = domain + "/client/membership/add-points"
        let params = [ "id": userId , "points" : points ] as [String : Any]
        authentification_headers{ authentification_headers in
            Alamofire.request(url, method: .post, parameters: params,encoding: JSONEncoding.default, headers: authentification_headers).responseJSON(queue: threadQueue)
            { response in
                if let serverResponse = ServerResponse(json: JSON(response.result.value)){
                    completion(serverResponse.status)
                }
            }
        }
    }
}

