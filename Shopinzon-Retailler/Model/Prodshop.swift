//
//  Prodshop.swift
//  Shopinzon
//
//  Created by Macho Uzan on 20/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class Prodshop {
    var id: UInt64
    var note: String
    var price: Float
    var product_id: String
    var shop_id: String
    required init?(json: JSON) {
        if let id = json["id"].uInt64,
            let price = json["price"].float,
            let note = json["note"].string{
            self.id = id
            self.note = note
            self.price = price
            if let product_id = json["product_id"].string {
                self.product_id = product_id

            }
            else if let product_id = json["product_id"].int {
                self.product_id = "\(product_id)"
            }
            else {
                return nil
            }
            if let shop_id = json["shop_id"].string {
                self.shop_id = shop_id
            }
            else if let shop_id = json["shop_id"].int {
                self.shop_id = "\(shop_id)"
            }
            else {
                return nil
            }
        }
        else
        {
            return nil
        }
    }

}
