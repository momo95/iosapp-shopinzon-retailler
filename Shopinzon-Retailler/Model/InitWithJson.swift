//
//  initWithJson.swift
//  Shopinzon
//
//  Created by Macho Uzan on 02/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
protocol InitWithJson {
    init?(json:JSON)
}
