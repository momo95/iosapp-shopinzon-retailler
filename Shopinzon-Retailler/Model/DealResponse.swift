//
//  DealResponse.swift
//  Shopinzon
//
//  Created by Macho Uzan on 09/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
enum DealResponseType:Int {
    case refused = 0
    case accepted = 1
}
class DealResponse : Response{
    var type:ResponseType = .deal
    var value:DealResponseType
    var time: Int
    var timeDate: Date {
        get {
            return Date(timeIntervalSince1970: TimeInterval(time)/1000)
        }
    }
    var json:[String: Any] {
        get {
            return [
                "type" : type.rawValue as Int,
                "value" : value.rawValue as Int
            ]
        }
    }
    init(value:DealResponseType,time:Int){
        self.time = time
        self.value = value
    }
}
