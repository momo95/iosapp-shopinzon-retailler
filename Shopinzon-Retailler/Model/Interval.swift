//
//  Interval.swift
//  Shopinzon
//
//  Created by Macho Uzan on 04/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation

class Interval {
    var openHours:HourMinuteTime
    var closeHours:HourMinuteTime
    init?(openMinutes:Int,closeMinutes:Int){
        if (openMinutes < closeMinutes){
            openHours = HourMinuteTime(minutes: openMinutes)!
            closeHours = HourMinuteTime(minutes: closeMinutes)!
        }
        else {
           return nil
        }
    }
    func contains(time:HourMinuteTime)->Bool{
        return (time >= openHours && time <= openHours)
    }
    var description: String {
        let str = "\(openHours)-\(closeHours)"
        return str
    }
}
