//
//  Position.swift
//  Shopinzon
//
//  Created by Macho Uzan on 19/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//
import Foundation
import SwiftyJSON
class Club{
    var member:Int
    var blue:Level
    var silver:Level
    var gold:Level
    var platinium:Level
    init(member:Int,bluePunches:Int,
         blue:Level,silver:Level,gold:Level,platinium:Level)
    {
        self.member = member
        self.blue = blue
        self.silver = silver
        self.gold = gold
        self.platinium = platinium
    }
    required init?(json: JSON) {
        if let member = json["member"].int,
            let blue = Level(json: json["blue"]),
            let silver = Level(json: json["silver"]),
            let gold = Level(json: json["gold"]),
            let platinium = Level(json: json["platinium"]){
            self.member = member
            self.blue = blue
            self.silver = silver
            self.gold = gold
            self.platinium = platinium
        }
        else
        {
            return nil
        }
    }
}
