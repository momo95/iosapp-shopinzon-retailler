//
//  Level.swift
//  Shopinzon
//
//  Created by Macho Uzan on 11/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class Level{
    var floor:Int
    var discount:Int
    init(floor:Int,discount:Int) {
        self.floor = floor
        self.discount = discount
    }
    required init?(json: JSON) {
        if let floor = json["floor"].int,
            let discount = json["discount"].int{
            self.floor = floor
            self.discount = discount
        }
        else
        {
            return nil
        }
    }
}
