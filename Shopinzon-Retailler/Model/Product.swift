//
//  Product.swift
//  Shopinzon
//
//  Created by Macho Uzan on 19/02/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit
class Product: NSObject,InitWithJson, NSCoding{
    var id:UInt64
    var name:String
    var brand:String
    var product_description:String?
    var picture:UIImage?
    var likes:Int?
    var liked:Bool?
    var saved:Bool?
    var shopPrice:Float?
    var categories:[Category]?
    var tags:[String]?
    var numberOfUsers:Int?
    var sizes:[String]?
    var price:Price?
    var gallery:[Media]?
    var embassador:String? {
        return gallery?.first?.url
    }
    init(id:UInt64,name:String,brand:String ,description:String,
         embassador:String,likes:Int ,liked:Bool,saved:Bool,
         shopPrice:Float?,categories:[Category],tags:[String],
         numberOfUsers:Int,sizes:[String]?,price:Price?,discount:Price?)
    {
        self.id = id
        self.name = name
        self.brand = brand
        self.product_description = description
        self.likes = likes
        self.liked = liked
        self.saved = saved
        self.shopPrice = shopPrice
        self.categories = categories
        self.gallery = [Media(url:embassador)]
        self.tags = tags
        self.sizes = sizes
        self.numberOfUsers = numberOfUsers
        self.price = price
    }
    required init?(json: JSON) {
        print(json)
        if let id = json["id"].uInt64,
            let name = json["name"].string,
            let brand = json["brand"].string,
            let description = json["description"].string,
            let tags = json["tags"].array?.map({$0.stringValue}){
            
            self.id = id
            self.name = name
            self.brand = brand
            self.tags = tags
            self.product_description = description
            
            if let gallery = json["gallery"].array?.map({Media(json: $0)!}){
                self.gallery = gallery
            }
            if let likes = json["likes"].int{
                self.likes = likes
            }
            if let liked = json["liked"].bool{
                self.liked = liked
            }
            if let saved = json["saved"].bool{
                self.saved = saved
            }
            if let categories = json["categories"].array?.map({Category(json: $0)!}){
                self.categories = categories
            }
            if let numberOfUsers = json["numberOfUsers"].int {
                self.numberOfUsers = numberOfUsers
            }
            if let price = Price(json: json["price"]){
                self.price = price
            }
            else{
                self.price = nil
            }
            if let sizes = json["sizes"].array?.map({$0.stringValue})
            {
                self.sizes = sizes
            }
            else{
                self.sizes = nil
            }
        }
        else{
            return nil
        }
        
    }
    func loadImage(){
        //make it return image if ther is
        if embassador != nil {
            getImageAsync(imageUrl: embassador!){ image in
                self.picture = image
            }
        }
    }
    // MARK: NSCoding
    required init(coder decoder: NSCoder) {
        //Error here "missing argument for parameter name in call
        self.id = decoder.decodeObject(forKey: "id") as! UInt64
        self.name = decoder.decodeObject(forKey: "name") as! String
        self.brand = decoder.decodeObject(forKey: "brand") as! String
        self.sizes = decoder.decodeObject(forKey: "sizes") as! [String]
        self.gallery = decoder.decodeObject(forKey: "gallery") as! [Media]
    }
    
    func encode(with: NSCoder) {
        with.encode(self.id, forKey: "id")
        with.encode(self.name, forKey: "name")
        with.encode(self.brand, forKey: "brand")
        with.encode(self.sizes, forKey: "sizes")
        with.encode(self.gallery, forKey: "gallery")
    }
}
