//
//  Context.swift
//  Shopinzon
//
//  Created by Macho Uzan on 01/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
@objc enum ContextType:Int {
    case product = 0
}
@objc protocol Context {
    var type:ContextType {get set}
    var json:[String: Any] {get}
}
