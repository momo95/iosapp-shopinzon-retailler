//
//  Order.swift
//  Shopinzon
//
//  Created by Macho Uzan on 05/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class Order {
    class User {
        var email:String
        var invoiceAddress:Address
        var FirstName:String
        var LastName:String
        var name:String?
        var picture:String?
        init(email:String,invoiceAddress:Address,FirstName:String,LastName:String) {
            self.email = email
            self.FirstName = FirstName
            self.LastName = LastName
            self.invoiceAddress = invoiceAddress
        }
        init?(json: JSON) {
            if let email = json["email"].string,
                let FirstName = json["invoiceFirstName"].string,
                let LastName = json["invoiceLastName"].string,
                let invoiceAddress = Address(json: json["invoiceAddress"]){
                self.email = email
                self.FirstName = FirstName
                self.LastName = LastName
                self.invoiceAddress = invoiceAddress
            }
            else
            {
                return nil
            }
        }
    }
    class ClubDiscount {
        var off:Percent
        var level:Int
        init(off:Percent,level:Int) {
            self.off = off
            self.level = level
        }
        init?(json: JSON) {
            if let off = json["off"].float,
                let level = json["level"].int{
                self.off = off
                self.level = level
            }
            else
            {
                return nil
            }
        }
    }
    enum State:Int {
        case Attente = 3
        case Preparation = 2
        case Prêt = 1
        case Finis = 0
        case Annulé = -1
    }
    var id : Long
    var time : Int
    var user : User
    var prodshop : Prodshop
    var size : String
    var payment : String
    var dealId: String
    var clubDiscount: ClubDiscount
    var state : State
    var paid : Float
    init(id : Long,time : Int,user : User,prodshop : Prodshop,size : String,payment : String,dealId: String,clubDiscount: ClubDiscount,state : State,paid : Float) {
        self.id = id
        self.time = time
        self.size = size
        self.user = user
        self.payment = payment
        self.dealId = dealId
        self.prodshop = prodshop
        self.clubDiscount = clubDiscount
        self.state = state
        self.paid = paid
    }
    required init?(json: JSON) {
        print(json)
        if let id = json["id"].uInt64,
            let time = json["time"].int,
            let size = json["size"].string,
            let payment = json["payment"].string,
            let dealId = json["dealId"].string,
            let prodshop = Prodshop(json: json["prodshop"]),
            let user = User(json: json["user"]),
            let clubDiscount = ClubDiscount(json: json["clubDiscount"]),
            let stateInt = json["state"].int,
            let paid = json["paid"].float,
            let state = State(rawValue: stateInt)
        {
            self.id = id
            self.time = time
            self.size = size
            self.user = user
            self.payment = payment
            self.dealId = dealId
            self.prodshop = prodshop
            self.state = state
            self.clubDiscount = clubDiscount
            self.paid = paid
        }
        else
        {
            return nil
        }
    }
}

