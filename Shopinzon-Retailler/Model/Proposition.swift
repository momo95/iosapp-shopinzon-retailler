//
//  Proposition.swift
//  Shopinzon
//
//  Created by Macho Uzan on 15/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
protocol Proposition : Message {
    var context:Context {get set}
    var price:Float {get set}
    var state:PropositionState {get}
}
enum PropositionState:Int {
    case accepted = 1
    case refused = 2
    case canceled = 3
}
