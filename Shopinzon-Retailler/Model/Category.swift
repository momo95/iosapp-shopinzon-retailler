//
//  Category.swift
//  Shopinzon
//
//  Created by Macho Uzan on 04/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
class Category:NSObject,NSCoding{
    var id:UInt64?
    var name:String?
    var parent_id:UInt64?
    
    init(id:UInt64,name:String,parent_id:UInt64?) {
        self.id = id
        self.name = name
        self.parent_id = parent_id
    }
    required init?(json: JSON) {
        if let id = json["id"].uInt64,
            let name = json["name"].string
        {
            self.id = id
            self.name = name
            if let parent_id = json["parent_id"].uInt64 {
                self.parent_id = parent_id
            }
        }
    }
    // MARK: NSCoding
    required init(coder decoder: NSCoder) {
        //Error here "missing argument for parameter name in call
        self.id = decoder.decodeObject(forKey: "id") as? UInt64
        self.name = decoder.decodeObject(forKey: "name") as? String
        self.parent_id = decoder.decodeObject(forKey: "parent_id") as? UInt64
    }
    
    func encode(with: NSCoder) {
        with.encode(self.id, forKey: "id")
        with.encode(self.name, forKey: "name")
        with.encode(self.parent_id, forKey: "parent_id")
    }
}
