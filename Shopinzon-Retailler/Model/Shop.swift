//
//  Shop.swift
//  Shopinzon
//
//  Created by Macho Uzan on 18/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit

class Shop {
    class Club{
        var cards:[Shop.ClubCard]
        init?(json:JSON) {
            if let jsonCards = json.array{
                self.cards = jsonCards.map({ClubCard(json : $0)!})
            }
            else {
                return nil
            }
        }
    }
    class ClubCard{
        class Level {
            var numberOfMembers:Int?
            var discount:Int//percentage
            var require:Int
            var level:Int
            init?(json:JSON) {
                if let discount = json["discount"].int,
                    let level = json["level"].int,
                    let require = json["require"].int {
                    if let numberOfMembers = json["numberOfMembers"].int {
                        self.numberOfMembers = numberOfMembers
                    }
                    self.require = require
                    self.discount = discount
                    self.level = level
                }
                else {
                    return nil
                }
            }
        }
        var id:Long
        var isActive:Bool
        var title:String
        var levels:[Level]
        init?(json:JSON) {
            if let id = json["id"].uInt64,
                let isActive = json["isActive"].bool,
                let title = json["title"].string,
                let jsonLevels = json["levels"].array {
                self.id = id
                self.isActive = isActive
                self.title = title
                self.levels = jsonLevels.map({ Level(json : $0)! })
            }
            else {
                return nil
            }
        }
    }
    
    var id:String
    var name:String
    var phone:String?
    var email:String?
    var address:Address?
    var logo:Media
    var location:Location?
    var schedules:[OpenCloseDay]?
    var shop_description:String?
    var price:Price?
    var currentDistance:Double?
    var gallery:[Media]?
    var stars:Int?
    var numberOfVote:Int?
    var catalog:[Product]?
    var _distance:Double?
    var embassador:String? {
        return gallery?.first?.url
    }
    
    init(id:String,name:String,embassador:String,logo:String,gallery:[String],phone:String, email:String,address:String,location:Location,
         schedules:[OpenCloseDay],description:String,price:Price?)
    {
        self.logo = Media(url:logo)
        self.id = id
        self.name = name
        self.phone = phone
        self.email = email
        self.address = Address()
        self.address?.formated_address = address
        self.gallery = [Media(url:embassador)]
        self.location = location
        self.schedules = schedules
        self.shop_description = description
        self.price = price
        self.catalog = []
    }
    required init?(json: JSON) {
        print(json)
        if let id = json["id"].uInt64,
            let name = json["name"].string,
            let phone = json["phone"].string,
            let address = Address(json: json["address"]),
            let location = Location(json: json["location"]),
            let logo = Media(json: json["logo"]),
            let description = json["description"].string
        {
            self.logo = logo
            self.id = "\(id)"
            self.name = name
            self.phone = phone
            self.address = address
            self.location = location
            self.logo = logo
            self.shop_description = description
            
            if let gallery = json["gallery"].array?.map({Media(json: $0)!}){
                self.gallery = gallery
            }
            else {
                let gallery = json["gallery"].string
                self.gallery = [Media(url:gallery!)]
            }
            if let price = Price(json: json["price"]){
                self.price = price
            }
            else {
                self.price = nil
            }
            if let schedules = json["open_hours"].array?.map({OpenCloseDay(json: $0)}){
                self.schedules = schedules as? [OpenCloseDay]
            }
            if let stars = json["stars"].int{
                self.stars = stars
            }
            if let numberOfVote = json["numberOfVote"].int{
                self.numberOfVote = numberOfVote
            }
            if let email = json["email"].string {
                self.email = email
            }
            if let prodshops = json["prodshops"].array {
                self.catalog = []
                for prodshop in prodshops {
                    let product = Product(json: prodshop["product"])
                    product?.price = Price(shop_id: self.id, product_id: product?.id, value: prodshop["price"].floatValue, discount: nil)
                    self.catalog?.append(product!)
                }
            }
        }
        else{
            return nil
        }
        
    }
    required init?(json: JSON,product_id:UInt64,price:Float) {
        //print(json)
        if let id = json["id"].uInt64,
            let name = json["name"].string,
            let phone = json["phone"].string,
            let email = json["email"].string,
            let address = Address(json: json["address"]),
            let location = Location(json: json["location"]),
            let description = json["description"].string
        {
            if let logo = Media(json: json["logo"]){
                self.logo = logo
            }
            else{
                let logo = json["logo"].string
                self.logo = Media(url:logo!)
            }
            self.id = "\(id)"
            self.name = name
            self.phone = phone
            self.email = email
            self.address = address
            self.location = location
            
            self.shop_description = description
            
            
            if let gallery = json["gallery"].array?.map({Media(json: $0)!}){
                self.gallery = gallery
            }
            else {
                let gallery = json["gallery"].string
                self.gallery = [Media(url:gallery!)]
            }
            if let stars = json["stars"].int{
                self.stars = stars
            }
            if let numberOfVote = json["numberOfVote"].int{
                self.numberOfVote = numberOfVote
            }
            self.price = Price(shop_id: self.id, product_id: product_id, value: price, discount: nil)
            
            if let schedules = json["open_hours"].array?.map({OpenCloseDay(json: $0)}){
                self.schedules = schedules as? [OpenCloseDay]
            }
        }
        else{
            return nil
        }
        
    }
    func distance(from:CLLocation)->Double{
        if _distance != nil {
            return _distance!
        }
        return self.location!.location.distance(from: from)
    }
    func isCurrentlyOpen()->Bool{
        if schedules != nil {
            let currentDay = Date().currentDayNumberOfWeek()! - 1
            let currentTime = HourMinuteTime.currentTime()
            for openCloseDay in schedules! {
                if (openCloseDay.day == currentDay && openCloseDay.contains(time: currentTime)){
                    return true
                }
            }
        }
        return false
    }
    init?(algoliaJson:JSON){
        print(algoliaJson)
        if let id = algoliaJson["objectID"].string,
            let name = algoliaJson["name"].string,
            let phone = algoliaJson["phone"].string,
            let email = algoliaJson["email"].string,
            let description = algoliaJson["description"].string
        {
            self.logo = Media(json: algoliaJson["logo"])!
            self.id = id
            self.name = name
            self.phone = phone
            self.email = email
            self.shop_description = description
            if let address = Address(algoliaJson: algoliaJson) {
                self.address = address
            }
            if let longitude = algoliaJson["_geoloc"]["lng"].double,
                let latitude = algoliaJson["_geoloc"]["lat"].double{
                self.location = Location(latitude: latitude, longitude: longitude)
            }
            if let stars = algoliaJson["stars"].int{
                self.stars = stars
            }
            if let gallery = algoliaJson["galery"].array?.map({Media(json: $0)!}){
                self.gallery = gallery
            }
        }
        else{
            return nil
        }
    }
}

