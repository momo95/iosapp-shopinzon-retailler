//
//  HourMinuteTime.swift
//  Shopinzon
//
//  Created by Macho Uzan on 04/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
class HourMinuteTime:CustomStringConvertible {
    var totalMinutes:Int = 0
    init?(minutes:Int){
        if (totalMinutes >= 0 && totalMinutes < 1440){
            totalMinutes = minutes
        }
        else {
            return nil
        }
    }
    var hour : Int {
        get {
            return totalMinutes / 60
        }
        set{
            if newValue < 23 {
                totalMinutes = newValue * 60 + totalMinutes % 60
            }
        }
    }
    var minute : Int {
        get {
            return totalMinutes % 60
        }
        set{
            if newValue < 60 {
                totalMinutes -= totalMinutes % 60
                totalMinutes += newValue
            }
        }
    }
    var description: String {
        let str = "\(hour.format(f: "02")):\(minute.format(f: "02"))"
        return str
    }
    static func currentTime()->HourMinuteTime{
        return HourMinuteTime(minutes: Date().currentHour()!*60 + Date().currentMinute()!)!
    }
    static func < (left: HourMinuteTime, right: HourMinuteTime) -> Bool{
        return left.totalMinutes < right.totalMinutes
    }
    static func > (left: HourMinuteTime, right: HourMinuteTime) -> Bool{
        return left.totalMinutes > right.totalMinutes
    }
    static func == (left: HourMinuteTime, right: HourMinuteTime) -> Bool{
        return left.totalMinutes == right.totalMinutes
    }
    static func <= (left: HourMinuteTime, right: HourMinuteTime) -> Bool{
        return left.totalMinutes <= right.totalMinutes
    }
    static func >= (left: HourMinuteTime, right: HourMinuteTime) -> Bool{
        return left.totalMinutes >= right.totalMinutes
    }
}
