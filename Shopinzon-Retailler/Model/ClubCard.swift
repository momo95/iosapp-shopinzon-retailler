//
//  ClubCard.swift
//  Shopinzon
//
//  Created by Macho Uzan on 13/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class ClubCard{
    var points:Int
    init(points:Int)
    {
        self.points = points
    }
    required init?(json: JSON) {
        if let points = json["points"].int{
            self.points = points
        }
        else {
            return nil
        }
    }
}
