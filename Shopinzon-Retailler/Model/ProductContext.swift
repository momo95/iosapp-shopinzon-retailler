//
//  ProductContext.swift
//  Shopinzon
//
//  Created by Macho Uzan on 02/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
class ProductContext : Context {
    var type: ContextType = .product
    var productId:UInt64
    var size:String?
    var json:[String: Any] {
        get {
            if size == nil {
                return [
                    "type" : type.rawValue as Int,
                    "productId" : productId as UInt64
                ]
            }
            else {
                return [
                    "type" : type.rawValue as Int,
                    "productId" : productId as UInt64,
                    "size" : size as! String
                ]
            }
        }
    }
    init(productId:UInt64,size:String? = nil) {
        self.productId = productId
        self.size = size
    }
}
