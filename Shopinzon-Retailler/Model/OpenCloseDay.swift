//
//  OpenCloseDay.swift
//  Shopinzon
//
//  Created by Macho Uzan on 24/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class OpenCloseDay {
    var day:Int
    var open:HourMinuteTime
    var close:HourMinuteTime
    init?(openMinutes:Int,closeMinutes:Int,day:Int){
        if (openMinutes < closeMinutes){
            self.open = HourMinuteTime(minutes: openMinutes)!
            self.close = HourMinuteTime(minutes: closeMinutes)!
            self.day = day
        }
        else {
            return nil
        }
    }
    required init?(json: JSON) {
        if let day = json["day"].int,
            let open = json["open"].int,
            let close = json["close"].int {
            self.day = day
            self.open = HourMinuteTime(minutes: open)!
            self.close = HourMinuteTime(minutes: close)!
        }
        else
        {
            return nil
        }

    }
    func contains(time:HourMinuteTime)->Bool{
        return (time >= open && time <= close)
    }
    var description: String {
        let str = "\(open)-\(close)"
        return str
    }
}
