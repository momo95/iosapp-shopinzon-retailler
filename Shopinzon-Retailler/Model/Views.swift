//
//  Views.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 18/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class Views {
    class Client {
        var user:User
        var membership : User.Membership
        var comment : Comment?
        var commented : Comment?
        var lastPurchaseDate : Timestamp?
        var numberOfSales : Int?
        var totalPaid : Float?
        init?(json:JSON) {
            print(json)
            if let user = User(json : json["user"]),
                let membership = User.Membership(json: json["membership"])
            {
                self.user = user
                self.membership = membership
                if let lastPurchaseDate = json["lastPurchaseDate"].int {
                    self.lastPurchaseDate = lastPurchaseDate
                }
                if let comment = Comment(json: json["comment"]) {
                    self.comment = comment
                }
                if let commented = Comment(json: json["commented"]) {
                    self.commented = commented
                }
                if let numberOfSales = json["numberOfSales"].int {
                    self.numberOfSales = numberOfSales
                }
                if let totalPaid = json["totalPaid"].float {
                    self.totalPaid = totalPaid
                }
            }
            else {
                return nil
            }
        }
    }
}
