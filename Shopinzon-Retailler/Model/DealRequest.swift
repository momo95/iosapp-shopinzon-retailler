//
//  DealRequest.swift
//  Shopinzon
//
//  Created by Macho Uzan on 26/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
class DealRequest:Message {
    var type: MessageType = .dealRequest
    var author: String
    var sent: Int
    var sentDate: Date {
        get {
            return Date(timeIntervalSince1970: TimeInterval(sent)/1000)
        }
    }
    var context:Context?
    var json:[String: Any] {
        get {
            return [
                "type" : type.rawValue as Int,
                "context" : context!.json as [String: Any]
            ]
        }
    }
    init(author: String,sent: Int,context:Context) {
        self.author = author
        self.sent = sent
        self.context = context
    }
    init(author: String,sent: Date,context:Context) {
        self.author = author
        self.sent = Int(Double(sent.timeIntervalSince1970)*1000)
        self.context = context
    }
    
    func dealRequest2textMessage()->MessageText {
        let message =  MessageText(deal: self)
        if let context = message.context as? ProductContext,
            let size = context.size {
            message.content = "Ce produit est-il disponible en taille \(size)?"
        }
        else {
            message.content = "Ce produit est-il disponible?"
        }
        return message
    }
}

