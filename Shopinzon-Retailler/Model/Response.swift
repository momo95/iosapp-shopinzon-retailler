//
//  Response.swift
//  Shopinzon
//
//  Created by Macho Uzan on 09/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
enum ResponseType:Int {
    case deal = 0
}
protocol Response {
    var time: Int {get set}
    var type: ResponseType {get set}
    var json:[String: Any] {get}
}
