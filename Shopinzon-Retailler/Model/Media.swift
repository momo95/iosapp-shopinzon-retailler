//
//  Media.swift
//  Shopinzon
//
//  Created by Macho Uzan on 25/04/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
class Media:NSObject,NSCoding,InitWithJson{
    var url:String = ""
    var type:MediaType = .image
    
    init(url:String,type:MediaType = .image) {
        self.url = url
        self.type = type
    }
    required init?(json: JSON) {
        if let url = json["url"].string
        {
            self.url = url
            if let type = json["type"].string {
                self.type = MediaType(rawValue: type)!
            }
            else {
                self.type = .image
            }
            
        }
        else {
            return nil
        }
    }
    // MARK: NSCoding
    required init(coder decoder: NSCoder) {
        //Error here "missing argument for parameter name in call
        self.url = decoder.decodeObject(forKey: "url") as! String
        self.type = MediaType(rawValue: decoder.decodeObject(forKey: "type") as! String)!
    }
    
    func encode(with: NSCoder) {
        with.encode(self.url, forKey: "url")
        with.encode(self.type.rawValue, forKey: "type")
    }
}
enum MediaType:String {
    case image = "image"
    case video = "video"
}
