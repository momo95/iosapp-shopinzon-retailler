//
//  Message.swift
//  Shopinzon
//
//  Created by Macho Uzan on 01/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
enum MessageType:Int {
    case messageText = 0
    case deal = 1
    case dealRequest = 2
}
protocol Message {
    var type:MessageType {get}
    var author:String {get set}
    var context:Context? {get set}
    var sent: Int {get set}
    var sentDate: Date{get}
    var json:[String: Any] {get}
}
