//
//  MessageText.swift
//  Shopinzon
//
//  Created by Macho Uzan on 02/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
class MessageText: Message {
    var type: MessageType = .messageText
    var author: String
    var sent: Int
    var sentDate: Date {
        get {
            return Date(timeIntervalSince1970: TimeInterval(sent)/1000)
        }
    }
    var content:String
    var context:Context?
    var json:[String: Any] {
        get {
            if context == nil {
                return [
                    "type" : type.rawValue as Int,
                    "content" : content as String
                ]
            }
            else {
                return [
                    "type" : type.rawValue as Int,
                    "context" : context!.json as [String: Any],
                    "content" : content as String
                ]
            }
        }
    }
    init(author: String,sent: Int,content:String,context:Context? = nil) {
        self.author = author
        self.sent = sent
        self.content = content
        self.context = context
    }
    init(author: String,sent: Date,content:String,context:Context? = nil) {
        self.author = author
        self.sent = Int(Double(sent.timeIntervalSince1970)*1000)
        self.content = content
        self.context = context
    }
    
    init(deal: DealRequest) {
        self.author = deal.author
        self.sent = deal.sent
        self.content = "Est ce que le produit 'En cours de chargement...' est disponible ?"
        self.context = deal.context
    }
    
    
}

