//
//  RatedComment.swift
//  Shopinzon
//
//  Created by Macho Uzan on 30/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class Comment {
    var stars: Int
    var title: String?
    var text: String?
    var _creationDate: UInt64
    var creationDate: Date {
        get {
            return Date(timeIntervalSince1970: TimeInterval(_creationDate)/1000)
        }
    }
    init() {
        title = ""
        _creationDate = 1023123
        text = ""
        stars = 0
    }
    var json:[String: Any] {
        get {
            var dic:[String : Any] = ["stars" : stars,"creationDate" : _creationDate]
            if (title != nil) {  dic["title"] = title }
            if (text != nil) { dic["text"] = text }
            return dic
        }
    }
    init(stars: Int, title: String? = nil, text: String? = nil, creationDate: UInt64 = 1023123) {
        self.title = title
        self._creationDate = creationDate
        self.text = text
        self.stars = stars
    }
    init?(json:JSON){
        if let stars = json["stars"].int,
            let creationDate = json["creationDate"].uInt64{
            self.stars = stars
            self.title = json["title"].string
            self.text = json["text"].string
            self._creationDate = creationDate
        }
        else {
            return nil
        }
    }
}
class Comment_with_author : Comment {
    var user_id:String
    var user_name : String
    override var json:[String: Any] {
        get { return [
            "user_id" : user_id as AnyObject,
            "user_name" : user_name as AnyObject,
            "stars" : stars as AnyObject,
            "title" : title as AnyObject,
            "text" : text as AnyObject,
            "creationDate" : _creationDate as AnyObject ]
        }
    }
    override init() {
        user_id = "1234"
        user_name = "Prénom Nom"
        super.init()
    }
    init(user_id:String,user_name:String, stars: Int, title: String, text: String, creationDate: UInt64 = 1023123) {
        self.user_id = user_id
        self.user_name = user_name
        super.init(stars: stars, title: title, text: text, creationDate: creationDate)
    }
    override init?(json:JSON){
        if let user_id = json["user_id"].string,
            let user_name = json["user_name"].string {
            self.user_id = user_id
            self.user_name = user_name
            super.init(json: json)
        }
        else {
            return nil
        }
    }
    
}
