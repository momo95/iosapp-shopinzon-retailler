//
//  User.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 17/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON
class User {
    class Membership{
        class State {
            var points:Int
            var creationDate:Timestamp
            init?(json:JSON) {
                if let points = json["points"].int,
                    let creationDate = json["creationDate"].int {
                    self.points = points
                    self.creationDate = creationDate
                }
                else {
                    return nil
                }
            }
        }
        var card:Shop.ClubCard
        var state:State
        init?(json:JSON) {
            if let card = Shop.ClubCard(json: json["card"]),
                let state = State(json: json["state"]) {
                self.card = card
                self.state = state
            }
            else {
                return nil
            }
        }
    }
    
    
    var id:String
    var name:String
    var phone:String?
    var email:String?
    var address:Address?
    var picture:String?
    
    init(id:String,name:String,phone:String, email:String,address:String,picture:String? = nil){
        self.picture = picture
        self.id = id
        self.name = name
        self.phone = phone
        self.email = email
        self.address = Address()
        self.address?.formated_address = address
    }
    required init?(json: JSON) {
        //print(json)
        if let id = json["id"].uInt64 {
            self.id = "\(id)"
        }
        else if let id = json["id"].string {
            self.id = id
        }
        else {
            return nil
        }
        if let name = json["name"].string
        {
            self.name = name
        }
        else {
            self.name = "Anonyme"
            
        }
        if let picture = json["picture"].string{
            self.picture = picture
        }
        if let phone = json["phone"].string{
            self.phone = phone
        }
        if let address = Address(json: json["address"]){
            self.address = address
        }
        if let email = json["email"].string {
            self.email = email
        }
        
    }
    
    
}
