//
//  Deal.swift
//  Shopinzon
//
//  Created by Macho Uzan on 25/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//


import Foundation
class Deal: Message {
    var type: MessageType = .deal
    var author: String
    var sent: Int
    var sentDate: Date {
        get {
            return Date(timeIntervalSince1970: TimeInterval(sent)/1000)
        }
    }
    var context:Context?
    var dealResponse:DealResponse?
    var originalPrice:Float
    var price:Float
    var expire:Date
    var json:[String: Any] {
        get {
            return [
                "type" : type.rawValue as Int,
                "context" : context!.json as [String: Any],
                "originalPrice" : originalPrice as Float,
                "price":price as Float,
                "expire" : Int(expire.timeIntervalSince1970) as Int
            ]
        }
    }
    init(author: String,sent: Int,context:Context,originalPrice:Float,price:Float,expire:Date,dealResponse:DealResponse? = nil) {
        self.author = author
        self.sent = sent
        self.context = context
        self.originalPrice = originalPrice
        self.price = price
        self.expire = expire
        self.dealResponse = dealResponse
    }
    init(author: String,sent: Date,context:Context,originalPrice:Float,price:Float,expire:Date,dealResponse:DealResponse? = nil) {
        self.author = author
        self.sent = Int(Double(sent.timeIntervalSince1970)*1000)
        self.context = context
        self.originalPrice = originalPrice
        self.price = price
        self.expire = expire
        self.dealResponse = dealResponse
    }
}

