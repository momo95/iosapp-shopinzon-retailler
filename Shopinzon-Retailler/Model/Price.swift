//
//  Price.swift
//  Shopinzon
//
//  Created by Macho Uzan on 19/02/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class Price:NSObject, NSCoding ,InitWithJson{
    var shop_id:String?
    var product_id:UInt64?
    var value:Float
    var discount:Float?
    init(shop_id:String?, product_id:UInt64?, value:Float,discount:Float?) {
        self.value = value
        if product_id != nil {
            self.product_id = product_id
        }
        self.shop_id = shop_id
        if discount != nil {
            self.discount = discount
        }
    }
    required init?(json: JSON) {
        if let shop_id = json["shop_id"].string,
            let product_id = json["product_id"].uInt64,
            let price = json["value"].float{
            self.value = price
            self.product_id = product_id
            self.shop_id = shop_id
            if let discount = json["discount"].float {
                self.discount = discount
            }
            else{
                self.discount = nil
            }
            
        }
        else
        {
            return nil
        }
    }
    required init?(json: JSON,shop_id: String) {
        if let product_id = json["product_id"].uInt64,
            let price = json["value"].float{
            self.value = price
            self.product_id = product_id
            self.shop_id = shop_id
            if let discount = json["discount"].float {
                self.discount = discount
            }
            else{
                self.discount = nil
            }
            
        }
        else
        {
            return nil
        }
    }
    required init(coder decoder: NSCoder) {
        //Error here "missing argument for parameter name in call
        self.shop_id = decoder.decodeObject(forKey: "shop_id") as? String
        self.product_id = decoder.decodeObject(forKey: "product_id") as? UInt64
        self.value = decoder.decodeFloat(forKey: "value")
        self.discount = decoder.decodeObject(forKey: "") as? Float
    }
    
    func encode(with: NSCoder) {
        with.encode(self.shop_id, forKey: "shop_id")
        with.encode(self.product_id, forKey: "product_id")
        with.encode(self.value, forKey: "value")
        with.encode(self.discount, forKey: "discount")
    }
}
