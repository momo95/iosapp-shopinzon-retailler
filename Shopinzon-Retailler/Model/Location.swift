//
//  Position.swift
//  Shopinzon
//
//  Created by Macho Uzan on 19/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//
import Foundation
import SwiftyJSON
import MapKit
class Location:InitWithJson{
    var latitude:Double
    var longitude:Double
    var location:CLLocation{
        get {
            return CLLocation(latitude: self.latitude, longitude: self.longitude)
        }
        set(newLocation) {
            self.latitude = newLocation.coordinate.latitude
            self.longitude = newLocation.coordinate.longitude
        }
    }
    init(latitude:Double, longitude:Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    required init?(json: JSON) {
        if let latitude = json["latitude"].double,
            let longitude = json["longitude"].double{
            self.latitude = latitude
            self.longitude = longitude
        }
        else
        {
            return nil
        }
    }
}
