//
//  Address.swift
//  Shopinzon
//
//  Created by Macho Uzan on 04/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//
import Foundation
import SwiftyJSON
import MapKit
class Address:InitWithJson{
    var country:String?
    var postal_code:String?
    var locality:String?
    var route:String?
    var street_number:String?
    var formated_address:String?
    
    init(){
        country = nil
        postal_code = nil
        locality = nil
        route = nil
        street_number = nil
        formated_address = nil
    }
    required init?(json: JSON) {
        print(json)
        self.locality = json["locality"].string
        self.country = json["country"].string
        self.street_number = json["streetNumber"].string
        self.route = json["route"].string
        self.postal_code = json["postalCode"].string
        if let formated_address = json["formatedAddress"].string {
            self.formated_address = formated_address
        }
        else {
            self.formated_address = "\(street_number ?? "") \(route ?? ""),\(postal_code ?? ""),\(locality ?? ""),\(country ?? "")"
        }
        
    }
    init?(algoliaJson json: JSON){
        if let locality = json["locality"].string,
            let country = json["country"].string,
            let street_number = json["street_number"].string,
            let route = json["route"].string,
            let postal_code = json["postal_code"].string
        {
            self.locality = locality
            self.country = country
            self.street_number = street_number
            self.route = route
            self.postal_code = postal_code
            if let formated_address = json["formated_address"].string {
                self.formated_address = formated_address
            }
            else {
                self.formated_address = "\(street_number) \(route),\(postal_code),\(locality),\(country)"
            }
        }
        else
        {
            return nil
        }
        
    }
}
