//
//  MyShop.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 17/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseDatabase

class MyShop {
    
    static var id : String?  {
        get {
            return shop?.id
        }
    }
    static var catalog : [Product] = []
    static var shop:Shop? = nil
    
    static func refresh(completion:@escaping (_ shop:Shop,_ catalog:[Product]) -> Void){
        Server.me { (shop, catalog) in
            MyShop.shop = shop
            MyShop.catalog = catalog
            completion(shop, catalog)
        }
    }
    static func getId(completion:@escaping (_ id:String) -> Void){
        if let id = id {
            completion(id)
        }
        else {
            Server.me { (shop, catalog) in
                MyShop.shop = shop
                MyShop.catalog = catalog
                completion(shop.id)
            }
        }
        
    }
}
