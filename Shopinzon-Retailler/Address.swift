//
//  Address.swift
//  Shopinzon
//
//  Created by Macho Uzan on 04/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//
import Foundation
import SwiftyJSON
import MapKit
class Address:InitWithJson{
    var country:String?
    var postal_code:String?
    var locality:String?
    var route:String?
    var street_number:String?
    var formated_address:String?
    
    init(){
        country = nil
        postal_code = nil
        locality = nil
        route = nil
        street_number = nil
        formated_address = nil
    }
    required init?(json: JSON) {
        if let locality = json["locality"].string,
            let country = json["country"].string,
            let street_number = json["street_number"].string,
            let route = json["route"].string,
            let postal_code = json["postal_code"].string,
            let formated_address = json["formated_address"].string
        {
            self.locality = locality
            self.country = country
            self.street_number = street_number
            self.route = route
            self.formated_address = formated_address
            self.postal_code = postal_code
        }
        else
        {
            return nil
        }
    }
    init?(algoliaJson: JSON){
        if let locality = algoliaJson["city"].string,
            let country = algoliaJson["country"].string,
            let postal_code = algoliaJson["postal_code"].string,
            let formated_address = algoliaJson["street_address"].string
        {
            self.locality = locality
            self.country = country
            self.postal_code = postal_code
            self.formated_address = "\(formated_address),\(postal_code),\(locality),\(country)"
        }
        else
        {
            return nil
        }
        
    }
}

