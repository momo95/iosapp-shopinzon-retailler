//
//  DiscussionsList.swift
//  Shopinzon
//
//  Created by Macho Uzan on 03/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
class DiscussionsList {
    
    private var _dictionary:[String: [Message]]
    var dictionary:[String: [Message]] {
        get {
            return _dictionary
        }
    }
    var count:Int {
        get {
            return self.array().count
        }
    }
    var keys:[String] {
        get {
            return Array(dictionary.keys)
        }
    }
    typealias AddMessageClosure = ((_ id:String,_ message:Message?)->())
    private var onAdd: AddMessageClosure?
    
    typealias ChangeMessageClosure = ((_ id:String,_ message:Message?,_ ind:Int)->())
    private var onChange: ChangeMessageClosure?
    
    init() {
        _dictionary = [String: [Message]]()
    }
    func fetchOnChange(onChange:@escaping ChangeMessageClosure){
        self.onChange = onChange
    }
    func fetchOnAdd(onAdd:@escaping AddMessageClosure){
        self.onAdd = onAdd
    }
    func append(id:String,message:Message){
        if let _ = _dictionary[id] {
            _dictionary[id]!.append(message)
        }
        else {
            _dictionary[id] = [message]
        }
        onAdd?(id,message)
    }
    func modify(id:String,message:Message) {
        if let messages = _dictionary[id] {
            if let ind = messages.index(where: {$0.sent == message.sent}){
                _dictionary[id]![ind] = message
                onChange?(id,message,ind)
            }
        }
    }
    func add(id:String,messages:[Message]){
        self[id] = messages
    }
    
    func clean(){
        _dictionary = [:]
        self.onChange = nil
        self.onAdd = nil
    }
    func array()->Array<(key:String,value:[Message])>{
        return Array(_dictionary).sorted(by: { (first, second) -> Bool in
            if let firstLast = first.value.last?.sent,
                let secondLast = second.value.last?.sent {
                return firstLast > secondLast
            }
                else {
                    return true
            }
        })
    }
    
    subscript(row: Int)->(key:String,value:[Message]){
        var ret = self.array()[row]
        ret.value = self[ret.key]!
        return ret
    }
    subscript(id: String) -> [Message]?{
        get {
            if let _ = dictionary[id] {
                return dictionary[id]!
            }
            else {
                _dictionary[id] = []
                return dictionary[id]!
            }
        }
        set {
            _dictionary[id] = newValue
            onAdd?(id,nil)
        }
    }
}

