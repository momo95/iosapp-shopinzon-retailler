//
//  AutoMap.swift
//  Shopinzon
//
//  Created by Macho Uzan on 28/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
class AutoMap <T> {
    private var _dictionary:[Int: T]
    private var index:Int = 0
    var dictionary:[Int: T] {
        get {
            return _dictionary
        }
    }
    init() {
        _dictionary = [Int: T]()
    }
    var count:Int {
        get {
            return self.keys.count
        }
    }
    var keys:[Int] {
        get {
            return Array(dictionary.keys)
        }
    }
    func store(_ value:T)->Int{
        index = index + 1
        _dictionary[index] = value
        return index
    }
    func pop(_ key:Int)->T?{
        let value  = _dictionary[index]
        _dictionary[index] = nil
        return value
    }
    subscript(row: Int)->T?{
        var ret = dictionary[row]
        return ret
    }
}
