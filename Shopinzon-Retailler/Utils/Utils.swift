//
//  AsyncImageLoading.swift
//  Shopinzon
//
//  Created by Macho Uzan on 29/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON
import SVGKit
import MapKit
import AVFoundation
import AudioToolbox


let screenHeight = UIScreen.main.bounds.size.height
let screenWidth = UIScreen.main.bounds.size.width
let image_loading_queue = DispatchQueue(label: "image_loading_queue.response", qos: .utility, attributes: [.concurrent])

let downloader = ImageDownloader(
    configuration: ImageDownloader.defaultURLSessionConfiguration(),
    downloadPrioritization: .lifo,
    maximumActiveDownloads: 10,
    imageCache: AutoPurgingImageCache()
)
func vibrate(){
    DispatchQueue.main.async{
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
}
func vibrate(i:Int) {
    
    print("Running \(i)")
    switch i {
    case 1:
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
        
    case 2:
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        
    case 3:
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
    case 4:
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    case 5:
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
        
    case 6:
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
    default:
        let generator = UISelectionFeedbackGenerator()
        generator.selectionChanged()
    }
}
func addShadowToView(view: UIView){
    let shadowPath = UIBezierPath(rect: view.bounds)
    view.layer.masksToBounds = false
    view.layer.shadowColor = UIColor.black.cgColor
    view.layer.shadowOffset = CGSize(width: 0, height: 0.5)
    view.layer.shadowOpacity = 0.2
    view.layer.shadowPath = shadowPath.cgPath
}

func isInInterval(array:[Int],value:Int)->Bool{
    for i in stride(from: 0, to: array.count, by: 2){
        let open = array[i]
        let close = array[i+1]
        if (value >= open && value <= close){
            return true
        }
    }
    return false
}
func dayOfWeekFor(minutes:Int)->Int{
    switch minutes {
    case 0...1439 :
        return 0
    case 1440...2879 :
        return 1
    case 2880...4319 :
        return 2
    case 4320...5759 :
        return 3
    case 5760...7199 :
        return 4
    case 7200...8639 :
        return 5
    case 8640...10079 :
        return 6
    default:
        return -1
    }
}
func minutesFromDayBegin(minutesFromWeekBegin:Int)->Int{
    let day = dayOfWeekFor(minutes:minutesFromWeekBegin)
    let minutes = minutesFromWeekBegin - (day * 24 * 60)
    return minutes
}
func minutesFromWeekBegin(day:Int,minutesFromDayBegin:Int)->Int{
    let minutes = minutesFromDayBegin + (day * 24 * 60)
    return minutes
}
func getIntervalsFromMinutes(openMinutes:Int,closeMinutes:Int)->[Interval]{
    let dayOfOpen = dayOfWeekFor(minutes:openMinutes)
    let dayOfClose = dayOfWeekFor(minutes:closeMinutes)
    if dayOfOpen == dayOfClose {
        let interval = Interval(openMinutes: minutesFromDayBegin(minutesFromWeekBegin: openMinutes),closeMinutes: minutesFromDayBegin(minutesFromWeekBegin: closeMinutes))
        return [interval!]
    }
    else {
        let firstInterval = Interval(openMinutes: minutesFromDayBegin(minutesFromWeekBegin: openMinutes),closeMinutes: 1439)
        var ret:[Interval] = [firstInterval!]
        ret.append(contentsOf: getIntervalsFromMinutes(openMinutes: minutesFromWeekBegin(day: dayOfOpen+1,minutesFromDayBegin: 0), closeMinutes: closeMinutes))
        return ret
    }
}
func getImageAsync(imageUrl:String,threadQueue:DispatchQueue = image_loading_queue,completion: @escaping (_ image: UIImage) -> Void)
{
    let urlRequest = URLRequest(url: URL(string: imageUrl)!)
    downloader.download(urlRequest) { response in
        switch response.result {
        case .success(let image):
            image.af_inflate()
            completion(image)
        case .failure(let error):
            print("Request failed with error: \(error)")
        }
        return
    }
}
func productOnImageView(product:Product,imageView:UIImageView,indicator:UIActivityIndicatorView,threadQueue:DispatchQueue)
{
    if product.embassador != nil {
        getImageAsync(imageUrl: product.embassador!) { image in
            DispatchQueue.main.async{
                indicator.removeFromSuperview()
                imageView.image = image
                product.picture = image
            }
        }
    }
}
func shoplogoOnImageView(shop:Shop,imageView:UIImageView,threadQueue:DispatchQueue = image_loading_queue)
{
    imageView.layer.cornerRadius = imageView.frame.size.width / 2;
    imageView.clipsToBounds = true
    let indicator:UIActivityIndicatorView  = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    indicator.startAnimating()
    imageView.addSubview(indicator)
    imageView.af_setImage(withURL: URL(string: shop.logo.url)!) { (image) in
        DispatchQueue.main.async{
            indicator.removeFromSuperview()
        }
    }
}
func shopEmbassadorOnImageView(shop:Shop,imageView:UIImageView,threadQueue:DispatchQueue = image_loading_queue,heightConstraint:NSLayoutConstraint){
    let indicator:UIActivityIndicatorView  = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    indicator.startAnimating()
    indicator.center = imageView.center
    imageView.addSubview(indicator)
    imageView.af_setImage(withURL: URL(string: shop.embassador!)!) { (image) in
        DispatchQueue.main.async{
            indicator.removeFromSuperview()
            let shop_embassador_height = getPictureHeight(picture: imageView.image!, width: screenWidth)
            heightConstraint.constant = shop_embassador_height
        }
    }
}

func getJSONSync(urlToRequest: String) -> NSData?{
    do
    {
        return try NSData(contentsOf: NSURL(string: urlToRequest) as! URL)
    }
    catch
    {
        return nil
    }
}
func svg2Image(svgName:String,size:CGSize,color:UIColor)->UIImage{
    let imageData = NSDataAsset(name: svgName)
    let SVGImage: SVGKImage = SVGKImage(data: imageData?.data)
    SVGImage.scaleToFit(inside: size)
    SVGImage.setTintColor(color: color)
    return SVGImage.uiImage
}
func svgOnImageView(svgName:String,imageView:UIImageView,color:UIColor){
    imageView.image = svg2Image(svgName:svgName,size:imageView.frame.size,color:color)
}
func getPictureHeight(picture:UIImage?,width:CGFloat) -> CGFloat{
    if picture == nil {
        return width
    }
    else{
        let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
        let rect  = AVMakeRect(aspectRatio: picture!.size, insideRect: boundingRect)
        return rect.size.height
    }
}
func getPictureWidth(picture:UIImage?,height:CGFloat) -> CGFloat{
    if picture == nil {
        return height
    }
    else{
        let boundingRect =  CGRect(x: 0, y: 0, width: CGFloat(MAXFLOAT), height: height)
        let rect  = AVMakeRect(aspectRatio: picture!.size, insideRect: boundingRect)
        return rect.size.width
    }
}
func centerMapOnLocation(location: CLLocation,map:MKMapView) {
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,500, 500)
    map.setRegion(coordinateRegion, animated: true)
}

func addRoundedBorderTo(view:UIView,color:UIColor = UIColor.white){
    // corner radius
    view.layer.cornerRadius = 2.5
    // border
    view.layer.borderWidth = 1.0
    view.layer.borderColor = color.cgColor
    view.clipsToBounds = true
}
func crop(image: UIImage, withWidth width: Double, andHeight height: Double) -> UIImage? {
    
    if let cgImage = image.cgImage {
        
        let contextImage: UIImage = UIImage(cgImage: cgImage)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        var croppedContextImage: CGImage? = nil
        if let contextImage = contextImage.cgImage {
            if let croppedImage = contextImage.cropping(to: rect) {
                croppedContextImage = croppedImage
            }
        }
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        if let croppedImage:CGImage = croppedContextImage {
            let image: UIImage = UIImage(cgImage: croppedImage, scale: image.scale, orientation: image.imageOrientation)
            return image
        }
        
    }
    
    return nil
}
func buttonToCheckbox(_ button:UIButton){
    button.setImage(#imageLiteral(resourceName: "unchecked-checkbox"), for: .normal)
    button.setImage(#imageLiteral(resourceName: "checked-checkbox"), for: .selected)
}
func roundView(_ view:UIView){
    view.layer.cornerRadius = view.frame.size.width / 2;
    view.clipsToBounds = true
}
func shimmer(view: UIView) {
    let gradient = CAGradientLayer()
    gradient.startPoint = CGPoint(x: 0, y: 0)
    gradient.endPoint = CGPoint(x: 1, y: -0.02)
    gradient.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width*3, height: view.bounds.size.height)
    
    let lowerAlpha: CGFloat = 0.7
    let solid = UIColor(white: 1, alpha: 1).cgColor
    let clear = UIColor(white: 1, alpha: lowerAlpha).cgColor
    gradient.colors     = [ solid, solid, clear, clear, solid, solid ]
    gradient.locations  = [ 0,     0.3,   0.45,  0.55,  0.7,   1     ]
    
    let theAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
    theAnimation.duration = 2
    theAnimation.repeatCount = Float.infinity
    theAnimation.autoreverses = false
    theAnimation.isRemovedOnCompletion = false
    theAnimation.fillMode = kCAFillModeForwards
    theAnimation.fromValue = -view.frame.size.width * 2
    theAnimation.toValue =  0
    gradient.add(theAnimation, forKey: "animateLayer")
    view.layer.mask = gradient
}
