//
//  CursorList.swift
//  Shopinzon
//
//  Created by Macho Uzan on 19/02/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import SwiftyJSON

class CursorList<A : InitWithJson> {
    var cursor : String? = nil
    var list : [A] = []
    public init(cursor : CursorList<A>) {
        self.cursor = cursor.cursor
        self.list = cursor.list
    }
    public init(cursor:String?,list:[A]) {
        self.cursor = cursor
        self.list = list
    }
    init(){
        self.cursor = nil
        self.list = []
    }
    init?(json: JSON){
        if let list = json.array{
            for item in list {
                self.list.append(A(json: item)!)
            }
            if let cursor = json["cursor"].string{
                self.cursor = cursor
            }
            else{
                self.cursor = nil
            }
        }

    }

}
