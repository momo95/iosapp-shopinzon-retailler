//
//  FirebaseMessaging.swift
//  Shopinzon
//
//  Created by Macho Uzan on 01/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseDatabase

enum State:Int {
    case loading = 0
    case empty = 1
    case occupied = 2
}
class FirebaseMessaging {
    
    static let shared = FirebaseMessaging()
    
    let discussionRef: DatabaseReference = Database.database().reference().child("discussions")
    
    let usersRef: DatabaseReference = Database.database().reference().child("users")
    
    var myUsersRef: DatabaseReference?
    
    var myDiscussionsRef:[DatabaseReference] = []
    
    var uid:String?
    
    var state:State = .loading

    
    func state(completion:@escaping (_ state:State) -> Void){
        if myUsersRef == nil {
            MyShop.refresh(completion: { (shop, _) in
                self.myUsersRef = self.usersRef.child(shop.id)
                self.myUsersRef!.child("discussions").observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.hasChildren(){
                        self.state = .occupied
                    }else{
                        self.state = .empty
                    }
                    completion(self.state)
                })
            })
        }
        else if self.state != .loading {
            completion(self.state)
        }
        else {
            myUsersRef!.child("discussions").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.hasChildren(){
                    self.state = .occupied
                }else{
                    self.state = .empty
                }
                completion(self.state)
            })
        }
    }
    func observeDiscussions(completionOnAdd:@escaping (_ user_id:String,_ shop_id:String,_ message: Message) -> Void,completionOnChange:@escaping (_ user_id:String,_ shop_id:String,_ message: Message) -> Void){
        if myUsersRef == nil {
            MyShop.refresh(completion: { (shop, _) in
                self.myUsersRef = self.usersRef.child(shop.id)
                self.myUsersRef!.child("discussions").queryOrdered(byChild: "lastMessageTime").observe(.childAdded, with: { snapshot in
                    let key = snapshot.key
                    let messagesRef = self.discussionRef.child(key)
                    self.myDiscussionsRef.append(messagesRef)
                    self.observeReferencedDiscussion(messagesRef: messagesRef,completionOnAdd: completionOnAdd,completionOnChange : completionOnChange)
                })
            })
        }
        else {
            self.myUsersRef!.child("discussions").queryOrdered(byChild: "lastMessageTime").observe(.childAdded, with: { snapshot in
                let key = snapshot.key
                let messagesRef = self.discussionRef.child(key)
                self.myDiscussionsRef.append(messagesRef)
                self.observeReferencedDiscussion(messagesRef: messagesRef,completionOnAdd: completionOnAdd,completionOnChange : completionOnChange)
            })
        }
        
    }
}
