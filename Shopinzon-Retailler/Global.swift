//
//  Global.swift
//  Shopinzon
//
//  Created by Macho Uzan on 24/09/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
let wishlist_album_name = "Wishlist"
let search_placeholder = "QUE RECHERCHEZ-VOUS ?"
let domain = "https://beta-dot-retailer-dot-rcm55-bagshop.appspot.com/rest"
let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
let gcmMessageIDKey = "607058134897"
let automaticResponseDealTime = 3600 //1 hour
let tealishColor = UIColor(red: 35/255, green: 187/255, blue: 165/255, alpha: 1.0)
let orangeBuyedColor = UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1.0)
let blueLevelColor = UIColor(red: 74/255, green: 144/255, blue: 226/255, alpha: 1.0)
let silverLevelColor = UIColor(red: 199/255, green: 199/255, blue: 199/255, alpha: 1.0)
let goldLevelColor = UIColor(red: 237/255, green: 217/255, blue: 101/255, alpha: 1.0)
let platiniumLevelColor = UIColor(red: 167/255, green: 163/255, blue: 191/255, alpha: 1.0)
let levelsColorsArray = [blueLevelColor,silverLevelColor,goldLevelColor,platiniumLevelColor]
let levelsNameArray = ["blue","silver","gold","platinium"]
let defaultUserPicture = URL(string: "https://storage.googleapis.com/public-res/images/user.jpg")!
typealias Long = UInt64
typealias Timestamp = Int
typealias Percent = Float
enum State:Int {
    case loading = 0
    case empty = 1
    case occupied = 2
}
