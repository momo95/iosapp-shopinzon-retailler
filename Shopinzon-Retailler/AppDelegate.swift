//
//  AppDelegate.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 21/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    var tabBarController:UITabBarController?{
        didSet{
            refreshTabBarBadge()
            if let idAuthor = discussionWaitingForOpen {
                openDiscussionView(idAuthor)
            }
        }
    }
    var unreadMessageCounter = -1 {
        didSet{
            refreshTabBarBadge()
        }
    }
    var discussionWaitingForOpen:String?
    override init() {
        super.init()
        UIApplication.shared.registerForRemoteNotifications()
        FirebaseApp.configure()
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Messaging Push notification
        Messaging.messaging().delegate = self
        if let fcmToken = Messaging.messaging().fcmToken {
            print("Firebase registration token: \(fcmToken)")
        }
        self.initDiscussions()
        UIApplication.shared.applicationIconBadgeNumber = 0
        return true
    }
}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        var idAuthor:String? = nil
        // Change this to your preferred presentation option
        if let id = userInfo["idAuthor"] as? String {
            print("Recipient ID: \(idAuthor)")
            idAuthor = id
        }
        
        // Print full message.
        print(userInfo)
        completionHandler([.badge, .sound, .alert])
        
        DispatchQueue.main.async {
            self.openDiscussionView(idAuthor)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        var idAuthor:String? = nil
        if let id = userInfo["idAuthor"] as? String {
            print("Recipient ID: \(idAuthor)")
            idAuthor = id
        }
        
        // Print full message.
        print(userInfo)
        completionHandler()
        
        DispatchQueue.main.async {
            self.openDiscussionView(idAuthor)
        }
    }
}
extension AppDelegate : MessagingDelegate {
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("application(received remoteMessage: MessagingRemoteMessage)")
        DispatchQueue.main.async {
            self.openDiscussionView()
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let token = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
            }.joined()
        print("Device Token: \(token)")
    }
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        print("application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])")
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        var idAuthor:String? = nil
        if let id = userInfo["idAuthor"] as? String {
            print("Recipient ID: \(idAuthor)")
            idAuthor = id
        }
        
        // Print full message.
        print(userInfo)
        DispatchQueue.main.async {
            self.openDiscussionView(idAuthor)
        }
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // Let FCM know about the message for analytics etc.
        //Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        print("application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)")
        var idAuthor:String? = nil
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        if let idDiscussion = userInfo["idDiscussion"] {
            print("Discussion ID: \(idDiscussion)")
        }
        if let id = userInfo["idAuthor"] as? String {
            print("Recipient ID: \(idAuthor)")
            idAuthor = id
        }
        // Print full message.
        print("userInfo: \(userInfo)")
        
        completionHandler(UIBackgroundFetchResult.newData)
        switch application.applicationState {
        case .active:
            DispatchQueue.main.async {
                self.openDiscussionView(idAuthor)
            }
        case .background:
            DispatchQueue.main.async {
                self.openDiscussionView(idAuthor)
            }
        case .inactive:
            DispatchQueue.main.async {
                self.openDiscussionView(idAuthor)
            }
        }
    }
    // [END receive_message]
    func showAlertGlobally(_ alert: UIAlertController) {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.windowLevel = UIWindowLevelAlert
        alertWindow.rootViewController = UIViewController()
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
        }
    }
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Server.refreshPushToken(new: fcmToken) { (response) in
            print(response)
        }
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    func openDiscussionView(_ idAuthor:String?  = nil){
        if let tabBarController = tabBarController as? UITabBarController
        {
            discussionWaitingForOpen = nil
            tabBarController.selectedIndex = 0
            if  let navigationController = tabBarController.selectedViewController as? UINavigationController,
                let  listDiscussionView: ListDiscussionViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Discussions") as? ListDiscussionViewController
            {
                
                navigationController.popToRootViewController(animated: false)
                navigationController.pushViewController(listDiscussionView, animated: false)
                if let idAuthor = idAuthor,
                    let  chatViewController: ChatViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatViewController {
                    chatViewController.interlocutor_id = idAuthor
                    chatViewController.messages = []
                    navigationController.pushViewController(chatViewController, animated: false)
                }
            }
        }
        else {
            discussionWaitingForOpen = idAuthor
        }
    }
}
extension AppDelegate{
    func initDiscussions(then:(() -> Void)? = nil) {
        MyShop.getId { (id) in
            ChatManager.makeInstance(chatMemberId: id , isUser: false)
            ChatManager.shared.loadChats(fromTime: Date().millisecondsTimeIntervalSince1970(), number: 20, resolve: { (chats) in
                self.unreadMessageCounter += chats.reduce(0, { (ret, chat) -> Int in
                    return ret + chat.unreadUserCount
                })
            })
            ChatManager.shared.listenForNewChats(completion: { (_) in
                self.unreadMessageCounter += 1
            })
            then?()
        }
    }
    func refreshTabBarBadge(){
        if let tabBarController = tabBarController as? UITabBarController,
            let tabItems = tabBarController.tabBar.items {
            let tabItem = tabItems[0] as! UITabBarItem
            if self.unreadMessageCounter > 0 {
                DispatchQueue.main.async {
                    tabItem.badgeValue = "\(self.unreadMessageCounter)"
                }
            }
            else {
                DispatchQueue.main.async {
                    tabItem.badgeValue = nil
                }
            }
        }
    }
    func sendTextMessage(recipient:String,content:String,context:Context? = nil){
        if let user = Auth.auth().currentUser{
            let message = MessageText(author: user.uid, sent: Date() , content: content , context : context)
            Server.sendMessage(recipient: recipient, message: message) { (response) in
                print(response)
                
            }
        }
    }
    
    func sendDeal(recipient:String,
                  context:ProductContext,originalPrice:Float,price:Float,expire:Date){
        if let user = Auth.auth().currentUser{
            let message = Deal(author: user.uid, sent: Date(), context: context, originalPrice: originalPrice,price: price, expire: expire)
            Server.sendMessage(recipient: recipient, message: message) { (response) in
                print(response)
                
            }
        }
    }
    
    func sendDealRequest(recipient:String,content:String,context:ProductContext){
        if let user = Auth.auth().currentUser{
            let message = DealRequest(author: user.uid, sent: Date(), context: context)
            Server.sendMessage(recipient: recipient, message: message) { (response) in
                print(response)
            }
        }
    }
    func sendDeal(from dealRequest:DealRequest){
        if let user = Auth.auth().currentUser,
            let context = dealRequest.context as? ProductContext {
            Server.getProduct(product_id: context.productId, completion: { (product) in
                if let price = product?.shopPrice {
                    let message = Deal(author: user.uid, sent: Date(), context: context, originalPrice: price,price: price, expire: Date(timeIntervalSinceNow: TimeInterval(automaticResponseDealTime)))
                    Server.sendMessage(recipient: dealRequest.author, message: message) { (response) in
                        print(response)
                    }
                }
            })
        }
    }
    func rejectDeal(from dealRequest:DealRequest){
        let content = "Désolé, nous ne sommes pas en mesure de vous proposer un deal sur ce produit."
        sendTextMessage(recipient: dealRequest.author, content: content)
    }
}



