//
//  MyOrdersViewController.swift
//  Shopinzon
//
//  Created by Macho Uzan on 01/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit

class MyOrdersViewController:UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    //MARK: Properties
    var orders:[(order:Order,product:Product,user:Order.User)] = []
    
    //MARK: Outlet
    @IBOutlet weak var orders_tableView: UITableView!
    @IBOutlet weak var indicator:UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        Server.getOrderList(completion: { (orders) in
            self.orders = orders
            DispatchQueue.main.async {
                self.orders_tableView.reloadData()
                self.indicator.stopAnimating()
            }
        })
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath) as? OrderTableViewCell else {
            fatalError("The dequeued cell is not an instance of OrderTableViewCell.")
        }
        let item = orders[indexPath.row]
        cell.fill(order: item.order, product: item.product, user: item.order.user)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "ShowOrder", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 3 == orders.count{
            let last = orders.last!.order.time
            print(last)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            self.orders_tableView.tableFooterView = spinner
            self.orders_tableView.tableFooterView?.isHidden = false
            Server.getOrderList(from: UInt64(orders.last!.order.time),completion: { (orders) in
                self.orders.append(contentsOf: orders)
                DispatchQueue.main.async {
                    self.orders_tableView.reloadData()
                    self.indicator.stopAnimating()
                    spinner.stopAnimating()
                    self.orders_tableView.tableFooterView?.isHidden = true
                }
            })
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "ShowOrder",
            let selection = self.orders_tableView.indexPathForSelectedRow {
                let orderViewController = segue.destination as! OrderViewController
                orderViewController.orderView = orders[selection.row]
        }
    }
}
