//
//  ChatMessageself.swift
//  Shopinzon
//
//  Created by Macho Uzan on 17/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase

class ChatMessageCell: UICollectionViewCell {
    //MARK: Outlet
    @IBOutlet weak var message_text_view: UITextView!
    
    @IBOutlet weak var text_bubble_view: UIView!
    
    @IBOutlet weak var date_label: UILabel!
    
    @IBOutlet weak var user_name_label: UILabel!
    
    var productName:String?
    var size:String?
    
    //MARK: Method
    func fill(message:MessageText,author:User?){
        DispatchQueue.main.async {
            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy HH:mm")
            self.date_label.text = dateFormatter.string(from: message.sentDate )
            self.message_text_view.text = message.content
            self.adaptFrame(message:message)
            if let name = author?.name {
                self.user_name_label.text = name
            }
            else {
                self.user_name_label.text = ""
            }
        }
    }
    
    func adaptFrame(message:MessageText){
        let size = CGSize(width: screenWidth*0.545,height: CGFloat(MAXFLOAT))
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin).union(.usesDeviceMetrics)
        let estimatedFrame =  NSString(string: message.content).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 17.0)!], context: nil)
        let estimatedWidth = max(150,ceil(estimatedFrame.width))
        let estimatedHeight = ceil(estimatedFrame.height) + 23
        //        if  !message.isSender {
        
        if message.author != MyShop.id {
            self.text_bubble_view.frame = CGRect(x: 16.5 ,y: 0,width: estimatedWidth + 30,height: estimatedHeight + 80)
            self.text_bubble_view.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            self.user_name_label.frame = CGRect(x : 15 ,y: 15,width: estimatedWidth ,height: 17)
            self.message_text_view.frame = CGRect(x : 15 ,y: 40,width: estimatedWidth ,height: estimatedHeight + 22)
            self.date_label.frame = CGRect(x : 15 ,y: 40 + estimatedHeight + 11,width: estimatedWidth ,height: 19)
            
            date_label.textAlignment = .left
            user_name_label.alpha = 1
        } else {
            //outgoing sending message
            
            self.text_bubble_view.frame = CGRect(x:  screenWidth - estimatedWidth - 45 ,y: 0,width: estimatedWidth + 30,height: estimatedHeight + 50)
            self.text_bubble_view.backgroundColor = UIColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
            self.message_text_view.frame = CGRect(x : 15,y: 12,width: estimatedWidth ,height: estimatedHeight + 22)
            self.date_label.frame = CGRect(x : 15 ,y: 12 + estimatedHeight + 8,width: estimatedWidth ,height: 19)
            date_label.textAlignment = .right
            user_name_label.alpha = 0
        }
    }
}

