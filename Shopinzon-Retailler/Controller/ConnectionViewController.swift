//
//  LoginViewController.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 21/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase

class ConnectionViewController: UIViewController,UITextFieldDelegate {
    //MARK: Properties
    var logged = false
    var alert: UIAlertController!

    //MARK:  Outlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var email_textField: UITextField!
    @IBOutlet weak var password_textField: UITextField!
    
    //MARK: View Controller overriding
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    //MARK: Method
    func keyboardWillShow(notification :NSNotification){
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
        }
    }
    func keyboardWillHide(){
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === email_textField {
            password_textField.becomeFirstResponder()
        }
        if textField === password_textField {
            self.log_in(self)
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        }
        return false
    }
    
    func notifyUser(title: String, message: String, timeToDissapear: Int) -> Void
    {
        alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        appDelegate.showAlertGlobally(alert)
        // setting the NSTimer to close the alert after timeToDissapear seconds.
        _ = Timer.scheduledTimer(timeInterval: Double(timeToDissapear), target: self, selector: #selector(dismissAlert), userInfo: nil, repeats: false)
    }
    
    func dismissAlert(){
        self.alert.dismiss(animated: true, completion: nil)
    }
    //MARK: Action
    @IBAction func log_in(_ sender: Any) {
        if email_textField.text == "" || password_textField.text  == "" {
            self.notifyUser(title: "Erreur", message: "Veuillez remplir les champs demandés", timeToDissapear: 2)
            return
        }
        else {
            Server.login(email: email_textField.text!, password: password_textField.text!){ customToken in
                if let customToken = customToken {
                    Auth.auth().signIn(withCustomToken: customToken){ authentification_token in
                        if let user = Auth.auth().currentUser{
                            user.getTokenForcingRefresh(true) {idToken, error in
                                if error != nil {
                                    // Handle error
                                    return;
                                }
                                print(idToken!)
                                self.performSegue(withIdentifier: "Connected", sender: nil)
                            }
                        }
                    }
                }
                else {
                    self.notifyUser(title: "Erreur", message: "Une erreur a eu lieu lors de la connexion.Veillez vous reconnectez", timeToDissapear: 2)
                }
            }
        }
    }
    
}
