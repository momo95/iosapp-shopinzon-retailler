//
//  ClientTableViewCell.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 17/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit

class ClientTableViewCell: UITableViewCell {
    //MARK: Outlet
    

    
    @IBOutlet weak var entity_logo: UIImageView!
    @IBOutlet weak var club_view: UIView!
    @IBOutlet weak var profilBackground_view: UIView!
    @IBOutlet weak var clubPromotion_view: UIView!
    @IBOutlet weak var clubPromotion_label: UILabel!

    @IBOutlet weak var mail_label: UILabel!
    @IBOutlet weak var name_label: UILabel!

    @IBOutlet weak var last_order_label: UILabel!
    @IBOutlet weak var last_order_date_label: UILabel!
    
    
    func fill(clientView:Views.Client){
        entity_logo.image = nil
        if let picture = clientView.user.picture {
            entity_logo.af_setImage(withURL: URL(string: picture)!)
        }
        else {
            entity_logo.af_setImage(withURL: defaultUserPicture)
        }
        mail_label.text = clientView.user.email
        name_label.text = clientView.user.name
        if let lastPurchaseDate = clientView.lastPurchaseDate {
            let date = Date(timeIntervalSince1970: TimeInterval(lastPurchaseDate/1000))
            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
            last_order_date_label.displayedText = dateFormatter.string(from: date )
        }
        else {
            last_order_date_label.text = ""
            last_order_label.text = ""
        }
        let points = clientView.membership.state.points
        let levels = clientView.membership.card.levels
        let reachedLevels = levels.filter({$0.require <= points})
        if let maxLevelReach = reachedLevels.max(by: {$0.require < $1.require }) {
            clubPromotion_label.text = "-\(maxLevelReach.discount)%"
            club_view.backgroundColor = levelsColorsArray[maxLevelReach.level]
            clubPromotion_view.backgroundColor = levelsColorsArray[maxLevelReach.level]
        }
        roundView(entity_logo)
        roundView(club_view)
        roundView(profilBackground_view)
        roundView(clubPromotion_view)
    }
    
    func roundView(_ view:UIView){
        view.layer.cornerRadius = view.frame.size.width / 2;
        view.clipsToBounds = true
    }
}
