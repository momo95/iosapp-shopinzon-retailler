//
//  DealRequestCell.swift
//  Entityinzon-Retailler
//
//  Created by Macho Uzan on 03/07/2017.
//  Copyright © 2017 Macho. All rights reserved.
//
import UIKit

class DealRequestCell: UITableViewCell {
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var product_size: UILabel!
    @IBOutlet weak var date_label: UILabel!
    var deal:DealRequest?
    func fill(deal:DealRequest){
        self.deal = deal
        if let context = deal.context as? ProductContext {
            Server.getProduct(product_id: context.productId, completion: { (product) in
                DispatchQueue.main.async {
                    self.product_name.text = "\(product!.name)\n\(product!.brand)"
                    if let size = context.size {
                        self.product_size.text = "Taille \(size)"
                    }
                    else {
                        self.product_size.text = ""
                    }
                    self.product_embassador.af_setImage(withURL: URL(string:product!.embassador!)!)
                }
            })
        }
        let dateFormatter = DateFormatter()
        let hourFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
        hourFormatter.setLocalizedDateFormatFromTemplate("HHhmm")
        date_label.text = "Posté le \(dateFormatter.string(from: deal.sentDate)) à \(hourFormatter.string(from: deal.sentDate))"
    }
    @IBAction func proposeDeal(_ sender: UIButton) {
        sender.displaySpinner()
        appDelegate.sendDeal(from: deal!)
    }
}
