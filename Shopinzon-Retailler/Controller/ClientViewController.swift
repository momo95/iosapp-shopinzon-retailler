//
//  ClientViewController.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 19/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Cosmos

class ClientViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    var clientView:Views.Client?
    var _clientId:String?
    var clientId:String{
        get {
        if let id = clientView?.user.id {
            return id
            }
        else  {
            return _clientId!
            }
        }
        set(newValue){
            _clientId = newValue
        }
    }
    let titleLabel = UILabel()

    @IBOutlet weak var entity_logo: UIImageView!
    @IBOutlet var club_views: [UIView]!
    @IBOutlet var rounded_views: [UIView]!
    @IBOutlet var shadowed_views: [UIView]!
    @IBOutlet var totalPoints: [UILabel]!
    @IBOutlet var clubLevel_label: [UILabel]!

    @IBOutlet weak var clubPromotion_label: UILabel!
    @IBOutlet weak var club_view: UIView!
    
    
    @IBOutlet weak var firstName_label: UILabel!
    @IBOutlet weak var lastName_label: UILabel!
    @IBOutlet weak var last_order_label: UILabel!
    @IBOutlet weak var last_order_date_label: UILabel!
    
    @IBOutlet weak var comment_view: UIView!
    @IBOutlet weak var comment_rate: CosmosView!
    @IBOutlet weak var commentDate_label: UILabel!
    @IBOutlet weak var commentTitle_label: UILabel!
    @IBOutlet weak var commentText_label: UILabel!

    @IBOutlet weak var commented_view: UIView!
    @IBOutlet weak var commented_rate: CosmosView!
    @IBOutlet weak var commentText_TextField: UITextField!
    @IBOutlet weak var modifyCommented_button: UIButton!
    var modifyingCommented = false
    var orders:[(order:Order,product:Product,user:Order.User)] = []
    @IBOutlet weak var orders_collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight_constraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        if let client = clientView {
            fillView(clientView: client)
        }
        Server.getClient(userId: clientId) { (clientView) in
            self.fillView(clientView: clientView)
        }
        rounded_views.forEach({roundView($0)})
        shadowed_views.forEach({ addShadowToView(view:$0) })
    }
    func fillView(clientView:Views.Client){
        DispatchQueue.main.async {
            self.titleLabel.attributedText = self.getNavMutableString(firstPart: "Fiche client : ", secondPart: clientView.user.name)
            self.titleLabel.sizeToFit()
            self.navigationItem.titleView = self.titleLabel
            if let picture = clientView.user.picture {
                self.entity_logo.af_setImage(withURL: URL(string: picture)!)
            }
            else {
                self.entity_logo.af_setImage(withURL: defaultUserPicture)
            }
            let points = clientView.membership.state.points
            let levels = clientView.membership.card.levels
            let reachedLevels = levels.filter({$0.require <= points})
            if let maxLevelReach = reachedLevels.max(by: {$0.require < $1.require }) {
                self.clubPromotion_label.text = "-\(maxLevelReach.discount)%"
                self.club_views.forEach({ $0.backgroundColor = levelsColorsArray[maxLevelReach.level] } )
                self.totalPoints.forEach({ $0.displayedText = "\(points) pts"})
                self.clubLevel_label.forEach({ $0.displayedText = "Niveau \(levelsNameArray[maxLevelReach.level].capitalized)"})
            }
            self.firstName_label.text = clientView.user.name
            if let lastPurchaseDate = clientView.lastPurchaseDate {
                let date = Date(timeIntervalSince1970: TimeInterval(lastPurchaseDate/1000))
                let dateFormatter = DateFormatter()
                dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
                self.last_order_date_label.displayedText = dateFormatter.string(from: date )
            }
            else {
                self.last_order_date_label.text = ""
                self.last_order_label.text = ""
            }
            if let comment = clientView.comment {
                let dateFormatter = DateFormatter()
                dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
                self.commentDate_label.displayedText = dateFormatter.string(from: comment.creationDate )
                self.commentTitle_label.displayedText = comment.title
                self.commentText_label.displayedText = comment.text
                self.comment_rate.rating = Double(comment.stars)
            }
            else {
                self.comment_view.isHidden = true
            }
            if let commented = clientView.commented {
                self.commented_rate.rating = Double(commented.stars)
                self.commentText_TextField.displayedText = commented.text
            }
            self.orders_collectionView.displaySpinner()
            Server.getOrderListForUser(userId: clientView.user.id) { (orders) in
                self.orders = orders
                DispatchQueue.main.async {
                    self.orders_collectionView.removeSpinner()
                    self.orders_collectionView.reloadData()
                    var height = self.orders_collectionView.collectionViewLayout.collectionViewContentSize.height
                    self.collectionViewHeight_constraint.constant = height
                    self.view.layoutIfNeeded()
                }
            }
        }
        
    }
    func getNavMutableString(firstPart:String,secondPart:String)->NSMutableAttributedString{
        var myString = firstPart + secondPart
        var pos = 0
        var len = firstPart.characters.count
        let myMutableString = NSMutableAttributedString(string: myString)
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 18.0)!, range: NSRange(location:pos ,length: len))
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0), range: NSRange(location:pos,length:len))
        pos = pos + len
        len = myString.characters.count - pos
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Light", size: 18.0)!, range: NSRange(location:pos,length:len))
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0), range: NSRange(location:pos,length:len))
        return myMutableString
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = orders_collectionView.dequeueReusableCell(withReuseIdentifier: "ClientOrderCollectionViewCell", for: indexPath) as? ClientOrderCollectionViewCell else {
            fatalError("The dequeued cell is not an instance of OrderTableViewCell.")
        }
        let item = orders[indexPath.row]
        cell.fill(order: item.order, product: item.product)
        addShadowToView(view:cell)
        return cell
    }
    
    @IBAction func modifyCommented(_ sender: UIButton) {
        if modifyingCommented {// Comment modified
            commented_view.displaySpinner()
            Server.commentAClient(userId: (clientView?.user.id)!, comment: Comment(stars: Int(self.commented_rate.rating), title: nil, text: self.commentText_TextField.text)){ _ in
                DispatchQueue.main.async {
                    self.commented_view.removeSpinner()
                    self.modifyCommented_button.setTitle("Modifier", for: .normal)
                    self.commented_rate.settings.updateOnTouch = false
                    self.commented_rate.settings.starSize = 21
                    self.commented_rate.update()
                    self.commentText_TextField.isUserInteractionEnabled = false
                    self.commentText_TextField.resignFirstResponder()
                }
                self.modifyingCommented = false
            }
            
        }
        else {
            DispatchQueue.main.async {// Comment going to be modified
                self.modifyCommented_button.setTitle("Enregistrer", for: .normal)
                self.commented_rate.settings.updateOnTouch = true
                self.commented_rate.settings.starSize = 30
                self.commented_rate.update()
                self.commentText_TextField.isUserInteractionEnabled = true
                self.commentText_TextField.becomeFirstResponder()
            }
            modifyingCommented = true
        }
    }
    
    @IBAction func assignPoints(_ sender: UIButton) {
        var title = "Attribuer des points"
        if let name = clientView?.user.name {
            title = "Attribuer des points à \(clientView!.user.name)"
        }
        let alert = UIAlertController(title:  title, message: "Nombre de points à attribuer: ", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Attribuer", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            // Get 1st TextField's text
            if let textField = alert.textFields?[0],
                let text = textField.text,
                let points = Int(text)
                {
                    Server.addPointToClient(userId: self.clientId, points: points, completion: { (_) in
                        self.club_view.displaySpinner()
                        Server.getClient(userId: self.clientId) { (clientView) in
                            self.club_view.removeSpinner()
                            self.fillView(clientView: clientView)
                        }
                    })
            }
        }))
        alert.addAction(UIAlertAction(title: "Annuler", style: UIAlertActionStyle.default, handler: nil))
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.keyboardType = .numberPad
        })
        self.present(alert, animated: true, completion: nil)
    }
}
