//
//  ChatViewController.swift
//  Shopinzon
//
//  Created by Macho Uzan on 15/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import FirebaseCrash
import AKPickerView_Swift

class ChatViewController : UIViewController,UITextFieldDelegate
{
    //MARK: Properties
    var messages:[Message]!
    var _user_id : String?
    var user_interlocutor :User?
    var interlocutor_id : String!{
        get {
            if let id = user_interlocutor?.id {
                return id
            }
            else {
                return _user_id
            }
        }
        set(newValue){
            _user_id = newValue
        }
    }
    var sectionatedMessages:[[Message]] = []
    let dateFormatter = DateFormatter()
    var sending:Bool = false
    var headersProduct:[Product] = []
    lazy var slideInTransitioningDelegate = SlideInPresentationManager()
    var _shouldScrollToLastRow:Bool = true

    var allMessagesLoaded = false
    var loadingMessages = false
    var listenKey:Int?
    //MARK: Outlet
    @IBOutlet weak var new_message_textField: UITextField!
    @IBOutlet weak var newMessage_textField_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var message_collection_view: UICollectionView!
    
    //MARK: View Controller overriding
    override func viewDidLoad(){
        super.viewDidLoad()
        self.loadingMessages = true
        if messages.count == 0 {
            ChatManager.shared.loadMessages(interlocutorId: self.interlocutor_id, fromTime: Date().millisecondsTimeIntervalSince1970() , number : 20) { (messages) in
                if messages.count < 20 {
                    self.allMessagesLoaded = true
                }
                self.messages = messages.reversed()
                self.sectionatedMessages = self.sectionMessages(self.messages)
                self.loadingMessages = false
                let scrollContentSizeHeight = self.message_collection_view.contentSize.height;
                let scrollOffset = self.message_collection_view.contentOffset.y
                DispatchQueue.main.async{
                    self.message_collection_view.reloadData()
                    self.message_collection_view.collectionViewLayout.invalidateLayout()
                    let y = scrollOffset + self.message_collection_view.contentSize.height - scrollContentSizeHeight
                    self.message_collection_view.setContentOffset(CGPoint(x: 0, y: y), animated: false)
                    self.scrollToBottom()//Scroll to bottom at begin
                }
            }
        }
        sectionatedMessages = sectionMessages(messages)
        if let user = user_interlocutor {
            layoutNavigationTitle(myString:"Discussion avec " + user.name)
        }
        else {
            layoutNavigationTitle(myString:"Discussion")
            Server.getChatList(user_ids: [self.interlocutor_id], completion: { (users) in
                if let user = users.first {
                    self.user_interlocutor = user
                    DispatchQueue.main.async {
                        self.layoutNavigationTitle(myString:"Discussion avec " + user.name)
                        self.message_collection_view.reloadData()
                    }
                }
            })
        }
        keyboardWillHide()
        appDelegate.registerForPushNotifications()
    }
    func layoutNavigationTitle(myString:String){
        DispatchQueue.main.async{
            let titleLabel = UILabel()
            var pos = 0
            var len = 16 < myString.characters.count ? 16 : myString.characters.count
            let myMutableString = NSMutableAttributedString(string: myString)
            myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Light", size: 18.0)!, range: NSRange(location:pos ,length: len))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0), range: NSRange(location:pos,length:len))
            pos = pos + len
            len = myString.characters.count - pos
            myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 18.0)!, range: NSRange(location:pos,length:len))
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0), range: NSRange(location:pos,length:len))
            titleLabel.attributedText = myMutableString
            titleLabel.sizeToFit()
            self.navigationItem.titleView = titleLabel
            if self.sectionatedMessages.count > 0 {
                let lastSection = self.sectionatedMessages.count - 1
                if let deals:[Deal] = self.sectionatedMessages[lastSection].filter({ (message) -> Bool in
                    if message is Deal {
                        return true
                    }
                    return false
                }) as? [Deal] {
                    if let lastDeal = deals.last {
                        let headers = self.message_collection_view.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader) as! [ChatContextHeader]
                        if let header = headers.reversed().first(where: {$0.tag == lastSection }) {
                            DispatchQueue.main.async {
                                header.product_onlinePrice.displayedText = "\(lastDeal.price)"
                            }
                        }
                    }
                }
            }
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollToBottom()//Scroll to bottom at begin
        readMessages()
        appDelegate.registerForPushNotifications()
        NotificationCenter.default.addObserver(self, selector:#selector(self.readMessages), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        self.listenKey = ChatManager.shared.listenForNewMessagesFrom(interlocutorId: self.interlocutor_id) { (message) in
            if let firstSent = self.messages.first?.sent,
                let lastSent = self.messages.last?.sent,
                message.sent >= firstSent && message.sent <= lastSent,
                let index = self.messages.index(where: { $0.sent == message.sent }){
                self.messages[index] = message
            }
            else {
                self.messages.append(message)
            }
            DispatchQueue.main.async{
                self.sectionatedMessages = self.sectionMessages(self.messages)
                self.message_collection_view.reloadData()
                self.scrollToBottomAnimated()
                self.readMessages()
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        if let listenKey = listenKey {
            ChatManager.shared.removeListener(key: listenKey)
        }
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: Method
    func readMessages(){
        Server.onReadMessage(interlocutor: interlocutor_id) { (response) in
            print(response)
        }
    }
    func sectionMessages(_ messages: [Message])->[[Message]]{
        var arr:[Message] = []
        var sectionatedMessages:[[Message]] = []
        var context:ProductContext? = nil
        for message in messages {
            if message.context == nil {
                arr.append(message)
            }
            else if let productContext = message.context as? ProductContext {
                if context?.productId == productContext.productId {
                    arr.append(message)
                }
                else {
                    sectionatedMessages.append(arr)
                    arr = [message]
                }
                context = productContext
            }
            else {
                sectionatedMessages.append(arr)
                arr = [message]
            }
        }
        if !arr.isEmpty {
            sectionatedMessages.append(arr)
        }
        return sectionatedMessages
    }
    func keyboardWillShow(notification :NSNotification){
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            DispatchQueue.main.async{
                self.newMessage_textField_bottom_constraint.constant = CGFloat(keyboardSize.height)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func keyboardWillHide(){
        newMessage_textField_bottom_constraint.constant = 0
        DispatchQueue.main.async{
            self.newMessage_textField_bottom_constraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardDidShow(notification :NSNotification){
        scrollToBottomAnimated()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        self.message_collection_view.addGestureRecognizer(tap)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === new_message_textField {
            sendMessage(self)
        }
        return false
    }
    func scrollToBottomAnimated() {
        let lastSection = message_collection_view.numberOfSections
        
        if lastSection > 0 {
            
            let lastRow = message_collection_view.numberOfItems(inSection: lastSection - 1)
            
            if lastRow > 0 {
                
                let indexPath = IndexPath(row: lastRow - 1, section: lastSection - 1)
                
                self.message_collection_view.scrollToItem(at: indexPath, at: .top, animated: true)
            }
        }
    }
    func scrollToBottom() {
        
        let lastSection = message_collection_view.numberOfSections
        
        if lastSection > 0 {
            
            let lastRow = message_collection_view.numberOfItems(inSection: lastSection - 1)
            
            if lastRow > 0 {
                
                let indexPath = IndexPath(row: lastRow - 1, section: lastSection - 1)
                
                self.message_collection_view.scrollToItem(at: indexPath, at: .top, animated: false)
            }
        }
    }
    //MARK: Action
    @IBAction func sendMessage(_ sender: AnyObject) {
        if let textMessage = new_message_textField.text{
            sending = true
            appDelegate.sendTextMessage(recipient: interlocutor_id, content: textMessage)
            new_message_textField.text = ""
        }
    }
    @IBAction func textFieldDidChange(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.1, animations: {
            self.new_message_textField.invalidateIntrinsicContentSize()
        })
    }
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "ProposeDeal" {
            let dealProposeView = segue.destination as! DealProposeView
            slideInTransitioningDelegate.direction = .bottom
            slideInTransitioningDelegate.disableCompactHeight = false
            dealProposeView.transitioningDelegate = slideInTransitioningDelegate
            dealProposeView.modalPresentationStyle = .custom
            dealProposeView.user_destinat = interlocutor_id
            if let context = messages.reversed().first(where: {$0.context is ProductContext})?.context as? ProductContext {
                dealProposeView.product_id = context.productId
                dealProposeView.size = context.size
            }
        }
    }
    func loadMoreMessages(_ from:Int){
        ChatManager.shared.loadMessages(interlocutorId: self.interlocutor_id, fromTime: from , number : 20) { (messages) in
            if messages.count < 20 {
                self.allMessagesLoaded = true
            }
            var newMessages:[Message] = messages.reversed()
            if newMessages.last?.sent == self.messages.first?.sent {
                newMessages.removeLast()
            }
            newMessages.append(contentsOf: self.messages)
            self.messages = newMessages
            self.sectionatedMessages = self.sectionMessages(self.messages)
            self.loadingMessages = false
            let scrollContentSizeHeight = self.message_collection_view.contentSize.height;
            let scrollOffset = self.message_collection_view.contentOffset.y
            DispatchQueue.main.async{
                self.message_collection_view.reloadData()
                self.message_collection_view.layoutIfNeeded()
                let y = scrollOffset + self.message_collection_view.contentSize.height - scrollContentSizeHeight
                self.message_collection_view.setContentOffset(CGPoint(x: 0, y: y), animated: false)
            }
        }
    }
}
