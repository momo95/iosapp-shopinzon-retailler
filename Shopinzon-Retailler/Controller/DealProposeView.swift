//
//  DealProposeView.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 30/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
class DealProposeView : UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    
    //MARK: Properties
    var product_id:UInt64?
    var size:String?
    var user_destinat :String!
    var sizes:[String] = []
    var alert: UIAlertController!
    
    //MARK: Outlet
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var productName_label: UILabel!
    @IBOutlet weak var originalPrice_label: UILabel!
    @IBOutlet weak var proposedPrice_label: UITextField!
    @IBOutlet weak var selectedSize_picker: UIPickerView!
    @IBOutlet weak var selectedSize_label: UILabel!
    @IBOutlet weak var dealPropose_button: UIButton!
    
    @IBOutlet weak var dealBottomConstraint: NSLayoutConstraint!
    

    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        if product_id != nil {
            Server.getProduct(product_id: product_id!, completion: { (product) in
                DispatchQueue.main.async {
                    self.product_embassador.af_setImage(withURL: URL(string:product!.embassador!)!)
                    self.productName_label.text = "\(product!.name)\n\(product!.brand)"
                    if let sizes = product?.sizes {
                        self.sizes = sizes
                        self.selectedSize_picker.reloadAllComponents()
                        if let size = self.size ,
                            let row = sizes.index(of: size)
                        {
                            self.selectedSize_picker.selectRow(row, inComponent: 0, animated: false)
                        }
                    }
                    else {
                        self.selectedSize_picker.isHidden = true
                        self.selectedSize_label.isHidden = true
                    }
                    if let price = product?.shopPrice {
                        self.originalPrice_label.text = "\(price)"
                    }
                }
            })
        }
        else {
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    //MARK: Method
    func keyboardWillShow(notification :NSNotification){
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.25,
                               delay: 0,
                               options: UIViewAnimationOptions.curveEaseOut,
                               animations: {
                                self.dealBottomConstraint.constant = 23 + keyboardSize.height
                                self.view.layoutIfNeeded()
                },completion: nil )
            }
        }
    }
    func keyboardWillHide(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25,
                           delay: 0,
                           options: UIViewAnimationOptions.curveEaseOut,
                           animations: {
                            self.dealBottomConstraint.constant = 23
                            self.view.layoutIfNeeded()
            },completion: nil )
            
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sizes.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sizes[row]
    }
    @IBAction func sendDeal(_ sender: UIButton) {
        if let originalPrice_text = originalPrice_label.text,
            let originalPrice = Float(originalPrice_text),
            let proposedPrice_text = proposedPrice_label.text,
            let proposedPrice = Float(proposedPrice_text),
            let product_id = self.product_id {
            let size = sizes[self.selectedSize_picker.selectedRow(inComponent: 0)]
            let context = ProductContext(productId: product_id, size: size)
            appDelegate.sendDeal(recipient: user_destinat,  context: context, originalPrice: originalPrice, price: proposedPrice, expire: Date(timeIntervalSinceNow: 60))
            self.dismiss(animated: true, completion: nil)
        }
        else if self.product_id == nil {
            self.notifyUser(title: "Oups", message: "Vous avez oubliez de selectionnez un produit", timeToDissapear: 1)
        }
        else {
            self.notifyUser(title: "Oups", message: "Veuillez remplir le prix.", timeToDissapear: 1)
        }
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func notifyUser(title: String, message: String, timeToDissapear: Int) -> Void
    {
        alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true,completion: nil)
        
        // setting the NSTimer to close the alert after timeToDissapear seconds.
        _ = Timer.scheduledTimer(timeInterval: Double(timeToDissapear), target: self, selector: #selector(dismissAlert), userInfo: nil, repeats: false)
    }
    func dismissAlert(){
        self.alert.dismiss(animated: true, completion: nil)
    }
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "SelectProduct" {
            let selectionView = segue.destination as! ProductSelectionView
            selectionView.productView = self
        }
    }
}
extension DealProposeView : SelectionProduct {
    func selectedProduct(product_id: UInt64) {
        self.product_id = product_id
        self.viewWillAppear(true)
    }
    
    
}
