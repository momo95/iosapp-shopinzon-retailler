//
//  ClientOrderCollectionViewCell.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 19/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
class ClientOrderCollectionViewCell:UICollectionViewCell {
    //Product
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var product_description: UILabel!
    @IBOutlet weak var product_price: UILabel!
    @IBOutlet weak var product_size: UILabel!
    @IBOutlet weak var order_date: UILabel!
    
    func fill(order:Order,product:Product){
        product_embassador.af_setImage(withURL: URL(string: product.embassador!)!)
        product_description.displayedText = "\(product.name)\n\(product.brand)"
        product_price.displayedText =  "\(order.paid)€"
        product_size.displayedText = "Taille : \(order.size)"
        let orderDate = order.time
        let date = Date(timeIntervalSince1970: TimeInterval(orderDate/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
        order_date.displayedText = "Réglé le \(dateFormatter.string(from: date ))"
    }
}
