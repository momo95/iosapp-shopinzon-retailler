//
//  OrderTableViewCell.swift
//  Shopinzon
//
//  Created by Macho Uzan on 01/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//


import UIKit

class OrderTableViewCell : UITableViewCell{
    
    //Order Info
    @IBOutlet weak var order_number: UILabel!
    @IBOutlet weak var order_date: UILabel!

    //Shop
    @IBOutlet weak var shop_logo: UIImageView!
    @IBOutlet weak var shop_name: UILabel!
    @IBOutlet weak var shop_description: UILabel!
    
    //Product
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var product_description: UILabel!
    @IBOutlet weak var product_price: UILabel!
    @IBOutlet weak var product_size: UILabel!
    
    //State
    @IBOutlet weak var state_background: UITextField!
    @IBOutlet weak var state_textField: UITextField!
    @IBOutlet weak var total_label: UILabel!
    
    func fill(){
        order_number.text = "Facture n°0034"
        order_date.text = "12/12/2017"
        
        shop_logo.layer.cornerRadius = shop_logo.frame.size.width / 2;
        shop_logo.clipsToBounds = true
        shop_logo.af_setImage(withURL: URL(string: "https://storage.googleapis.com/media-siz/OrvvzWoAjngxUhAZnssE")!)
        shop_name.text = "Shop #01"
        shop_description.text = "48 rue Francis Davso 13008 Marseille\nTél. : 04 91 01 02 03"
        
        product_embassador.af_setImage(withURL: URL(string: "https://storage.googleapis.com/media-siz/QHYxjGPueLjQQUbNRJpF")!)
        product_description.text = "Chemisier anis femme\nBranding Name"
        product_price.text = "150€"
        product_size.text = "Taille : 32"
        
        state_background.backgroundColor = tealishColor
        
        let string = "Statut : \("En préparation")"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Light", size: 14.0)!, range: NSRange(location: 0, length: 8))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!, range: NSRange(location: 9, length: string.characters.count - 9))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: tealishColor, range: NSRange(location:0,length:string.characters.count))
        state_textField.attributedText = attributedString
        total_label.text = "150€"
    }
    func fill(order:Order,product:Product,user:Order.User){
        order_number.text = "Facture n°\(order.id)"
        let date = Date(timeIntervalSince1970: TimeInterval(order.time/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
        order_date.text = dateFormatter.string(from: date )
        
        shop_logo.layer.cornerRadius = shop_logo.frame.size.width / 2;
        shop_logo.clipsToBounds = true
        if let picture = user.picture {
            shop_logo.af_setImage(withURL: URL(string: picture)!)
        }
        else {
            shop_logo.af_setImage(withURL: defaultUserPicture)
        }
        shop_name.text = "\(user.FirstName) \(user.LastName)"
        shop_description.text = "\(user.invoiceAddress.formated_address!)"
        
        product_embassador.af_setImage(withURL: URL(string: product.embassador!)!)
        product_description.text = "\(product.name)\n\(product.brand)"
        product_price.text =  "\(order.paid)€"
        product_size.text = "Taille : \(order.size)"
        
        state_background.backgroundColor = tealishColor
        
        let string = "Statut : \(order.state)"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Light", size: 14.0)!, range: NSRange(location: 0, length: 8))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!, range: NSRange(location: 9, length: string.characters.count - 9))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: tealishColor, range: NSRange(location:0,length:string.characters.count))
        state_textField.attributedText = attributedString
        total_label.text =  "\(order.paid)€"
        
    }
}

