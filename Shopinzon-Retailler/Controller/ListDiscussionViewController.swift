//
//  ListDiscussionViewController.swift
//  Shopinzon
//
//  Created by Macho Uzan on 24/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase

class ListDiscussionViewController : UIViewController{
    
    //MARK: Properties
    var listenKey:Int?
    var chats:[Chat]  = []
    var interlocutors:[Int:String] = [:]
    var users:[String:User] = [:]

    let entityList_queue = DispatchQueue(label: "entityList_queue.response", qos: .utility, attributes: [.concurrent])
    
    //MARK: Outlet
    @IBOutlet weak var list_discussion_tableView:UITableView!
    @IBOutlet weak var noResult_label:UILabel!
    @IBOutlet weak var indicator:UIActivityIndicatorView!
    
    //MARK: View Controller overriding
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        appDelegate.registerForPushNotifications()
        self.list_discussion_tableView.reloadData()
        loadMessages()
    }
    func loadMessages(){
        ChatManager.shared.loadChats(fromTime: Date().millisecondsTimeIntervalSince1970(), number: 20, resolve: { (chats) in
            if chats.count > 0 {
                self.layoutFromState(state: .occupied)
            }
            else {
                self.layoutFromState(state: .empty)
            }
            self.chats = chats
            Server.getChatList(user_ids: chats.map({$0.userId}), completion: { (users) in
                for user in users{
                    self.users[user.id] = user
                }
                DispatchQueue.main.async {
                    self.list_discussion_tableView.reloadData()
                }
            })
        })
        self.listenKey = ChatManager.shared.listenForNewChats(completion: { (chat) in
            var chats = [chat]
            let filtredChats = self.chats.filter({$0.shopId != chat.shopId || $0.userId != chat.userId})
            chats.append(contentsOf: filtredChats)
            self.chats = chats
            DispatchQueue.main.async{
                self.list_discussion_tableView.reloadData()
            }
            appDelegate.unreadMessageCounter = 0
        })
    }
    override func viewDidAppear(_ animated: Bool) {
        appDelegate.unreadMessageCounter = 0
        self.list_discussion_tableView.reloadData()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let listenKey = listenKey {
            ChatManager.shared.removeListener(key: listenKey)
        }
    }
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "ShowDiscussion" {
            if let indexPath = list_discussion_tableView.indexPathForSelectedRow {
                let chatViewController = segue.destination as! ChatViewController
                let item = chats[indexPath.row]
                chatViewController.messages = item.messages
                chatViewController.interlocutor_id = "\(item.userId)"
                if let user = users[interlocutors[indexPath.row]!]{
                    chatViewController.user_interlocutor = user
                    chatViewController.messages = []
                }
            }
        }
    }
    func layoutFromState(state:State){
        if state == .empty {
            DispatchQueue.main.async {
                self.noResult_label.isHidden = false
                self.indicator.stopAnimating()
            }
            
        }
        else if state == .occupied {
            DispatchQueue.main.async {
                self.noResult_label.isHidden = true
                self.indicator.stopAnimating()
            }
        }
        else {
            DispatchQueue.main.async {
                self.noResult_label.isHidden = true
                self.indicator.startAnimating()
            }
        }
    }
    @IBAction func logout(){
        Server.logout()
        if let user = Auth.auth().currentUser{
            print("UID  : "+user.uid+"\n")
        }
        self.performSegue(withIdentifier: "NotConnected", sender: nil)
    }
}

extension ListDiscussionViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return chats.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = chats[indexPath.row]
        if let lastMessage = item.lastMessage as? DealRequest {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DealRequestCell", for: indexPath) as? DealRequestCell  else {
                fatalError("The dequeued cell is not an instance of ProductPresentationViewCell.")
            }
            cell.fill(deal: lastMessage)
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListDiscussionCell", for: indexPath) as? ListDiscussionCell  else {
                fatalError("The dequeued cell is not an instance of ProductPresentationViewCell.")
            }
            self.interlocutors[indexPath.row] = chats[indexPath.row].userId
            if let id = interlocutors[indexPath.row],
                let user = users[id] {
                cell.entity_name_label.text = user.name
                if let picture = user.picture,
                    let url = URL(string: picture) {
                    cell.entity_logo.af_setImage(withURL: url)
                }
                else {
                    cell.entity_logo.af_setImage(withURL: defaultUserPicture)
                }
            }
                
            else {
                cell.entity_logo.af_setImage(withURL: defaultUserPicture)
            }
            
            cell.entity_logo.layer.cornerRadius = cell.entity_logo.frame.size.width / 2;
            cell.entity_logo.clipsToBounds = true
            
            if let lastMessage = item.lastMessage as? Deal {
                if lastMessage.author == MyShop.id {
                    cell.last_message_text_view.text = "Vous avez proposé un Deal"
                }
                else {
                    cell.last_message_text_view.text = "Vous avez reçu un Deal"
                }
                let dateFormatter = DateFormatter()
                let hourFormatter = DateFormatter()
                dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
                hourFormatter.setLocalizedDateFormatFromTemplate("HH:mm")
                cell.last_message_date_label.text = "Posté le \(dateFormatter.string(from: lastMessage.sentDate)) à \(hourFormatter.string(from: lastMessage.sentDate))"
            }
            else {
                let lastMessage = item.lastMessage as! MessageText
                if lastMessage.author == Auth.auth().currentUser?.uid {
                    cell.last_message_text_view.text = "Vous : \(lastMessage.content)"
                }
                else {
                    cell.last_message_text_view.text = "\(lastMessage.content)"
                }
                
                cell.last_message_text_view.text = lastMessage.content
                let dateFormatter = DateFormatter()
                let hourFormatter = DateFormatter()
                dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
                hourFormatter.setLocalizedDateFormatFromTemplate("HH:mm")
                cell.last_message_date_label.text = "Posté le \(dateFormatter.string(from: lastMessage.sentDate)) à \(hourFormatter.string(from: lastMessage.sentDate))"
            }
            self.layoutFromState(state: .occupied)
            return cell
        }
    }
    func loadImageCell(entity:Shop,cell:ListDiscussionCell,indexPath:IndexPath){
        let indicator:UIActivityIndicatorView  = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.startAnimating()
        cell.contentView.addSubview(indicator)
        indicator.center = cell.entity_logo.center
        /*getImageAsync(imageUrl: entity.logo.url){ image in
         DispatchQueue.main.async{
         entity.logoPicture = image
         cell.entity_logo.image = image
         indicator.removeFromSuperview()
         }
         }*/
    }
    
    @IBAction func showDiscussion(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(), to: list_discussion_tableView)
        let indexPath = list_discussion_tableView.indexPathForRow(at:buttonPosition)
        list_discussion_tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
        performSegue(withIdentifier: "ShowDiscussion", sender: self)
    }
}


