//
//  ShopCatalogViewCell.swift
//  Shopinzon
//
//  Created by Macho Uzan on 05/06/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
class ShopCatalogViewCell : UICollectionViewCell{
    //MARK: Outlet
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var product_title: UILabel!
    @IBOutlet weak var product_price: UILabel!
}
