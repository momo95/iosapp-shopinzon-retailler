//
//  ChatDealCell.swift
//  Shopinzon
//
//  Created by Macho Uzan on 21/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit

class ChatDealCell:UICollectionViewCell {
    
    //MARK: Outlet
    @IBOutlet weak var product_title: UILabel!
    @IBOutlet weak var product_onlinePrice: UILabel!
    @IBOutlet weak var product_originalPrice: UILabel!
    @IBOutlet weak var deal_time: UIProgressView!
    @IBOutlet weak var remaining_time: UILabel!
    @IBOutlet weak var product_embassador: UIImageView!
    var timer = Timer()
    var deal:Deal?
    func fill(deal:Deal){
        self.deal = deal
        self.timer.invalidate()
        if let response = deal.dealResponse{
            if response.value == .refused {
                self.remaining_time.text = "Offre refusée"
            }
            else if response.value == .accepted{
                self.remaining_time.text = "Offre acceptée"
            }
        }
        else {
            self.setProgressBar(false)
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ChatDealCell.setProgressBar(_:)), userInfo: nil, repeats: true)
        }
        if let context = deal.context as? ProductContext {
            Server.getProduct(product_id: context.productId, completion: { (product) in
                DispatchQueue.main.async {
                    if let size = context.size {
                        self.product_title.text = "\(product!.name)\nTaille : \(size)"
                    }
                    else {
                        self.product_title.text = "\(product!.name)"
                    }
                    self.product_embassador.af_setImage(withURL: URL(string:product!.embassador!)!)
                }
            })
        }
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
        self.product_onlinePrice.text = "\(deal.price.format(f: ".2"))€"
        self.product_originalPrice.text = "au lieu de \(deal.originalPrice.format(f: ".2"))€"
    }
    @objc func setProgressBar(_ animated:Bool = true)    {
        DispatchQueue.main.async {
            let totalProgress =  Float(self.deal!.expire.seconds(from: self.deal!.sentDate))
            let currentProgress = Float(Date.init().seconds(from: self.deal!.sentDate))
            let progress = currentProgress / totalProgress
            if progress <= 1 {
                self.remaining_time.text = "Il reste \((self.deal!.expire.hours(from: Date.init())).format(f: ".2")):\((self.deal!.expire.minutes(from: Date.init()) % 60).format(f: ".2")):\((self.deal!.expire.seconds(from: Date.init()) % 60).format(f: ".2")) jusqu'à la fin du deal"
                self.deal_time.setProgress(progress, animated: animated)
                UIView.animate(withDuration: 1, animations: {
                    self.deal_time.layoutIfNeeded()
                })
            }
            else{
                self.remaining_time.text = "Offre expirée"
                self.timer.invalidate()
            }
        }
    }
    override func prepareForReuse() {
        DispatchQueue.main.async{
            self.product_embassador.image = nil
        }
    }
}
