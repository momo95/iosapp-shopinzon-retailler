//
//  ChatViewControllerCollectionView.swift
//  Shopinzon
//
//  Created by Macho Uzan on 23/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase

extension ChatViewController:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ChatContextHeader", for: indexPath) as! ChatContextHeader
        DispatchQueue.main.async {
            header.isHidden = true
        }
        header.tag = indexPath.section
        if let context = sectionatedMessages[indexPath.section].first?.context {
            if let context = context as? ProductContext{
                if let product = headersProduct.first(where: {$0.id == context.productId}) {
                    DispatchQueue.main.async {
                        header.isHidden = false
                        header.product_embassador.af_setImage(withURL: URL(string:product.embassador!)!)
                        header.product_title.text = product.name
                        header.product_shopPrice.text = ""
                        if let price = product.shopPrice {
                            header.product_shopPrice.text = "\(price)"
                        }
                    }
                }
                else {
                    Server.getProduct(product_id: context.productId, completion: { (product) in
                        DispatchQueue.main.async {
                            header.isHidden = false
                            header.product_embassador.af_setImage(withURL: URL(string:product!.embassador!)!)
                            header.product_title.text = product!.name
                            if let price = product?.shopPrice {
                                header.product_shopPrice.text = "\(price)"
                            }
                            if !self.headersProduct.contains(where: {$0.id == context.productId}) {
                                self.headersProduct.append(product!)
                            }
                        }
                    })
                }
            }
        }
        return header
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if sectionatedMessages[indexPath.section][indexPath.item] is MessageText {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatMessageCell", for: indexPath as IndexPath) as! ChatMessageCell
            let message = sectionatedMessages[indexPath.section][indexPath.item] as! MessageText
            if let user = user_interlocutor {
                cell.fill(message: message,author:user)
            }
            else {
                cell.fill(message: message,author:nil)
            }
            return cell
        }
        else if sectionatedMessages[indexPath.section][indexPath.item] is Deal {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatDealCell", for: indexPath as IndexPath) as! ChatDealCell
            let dealMessage = sectionatedMessages[indexPath.section][indexPath.item] as! Deal
            cell.fill(deal: dealMessage)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatDealRequestCell", for: indexPath as IndexPath) as! ChatDealRequestCell
            let message = sectionatedMessages[indexPath.section][indexPath.item] as! DealRequest
            cell.fill(message: message)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if sectionatedMessages[indexPath.section][indexPath.item] is MessageText {
            
            let message = sectionatedMessages[indexPath.section][indexPath.item] as! MessageText
            let size = CGSize(width: screenWidth*0.545,height: CGFloat(MAXFLOAT))
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin).union(.usesDeviceMetrics)
            let estimatedFrame = NSString(string: message.content).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 17.0)!], context: nil)
            let estimatedHeight = ceil(estimatedFrame.height) + 23
            var padding:CGFloat = 50
            if message.author == Auth.auth().currentUser?.uid {
                padding = 80
                sending = false
            }
            return CGSize(width : view.frame.width, height : estimatedHeight + padding)
        }
        else if sectionatedMessages[indexPath.section][indexPath.item] is DealRequest {
            if indexPath.section == sectionatedMessages.count - 1 && indexPath.item == sectionatedMessages[indexPath.section].count - 1 {
                return CGSize(width : view.frame.width, height : 260)
            }
            return CGSize(width : view.frame.width, height : 150)
        }
        else { //if sectionatedMessages[indexPath.section][indexPath.item] is Deal
            return CGSize(width : view.frame.width, height : 300)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if cell is ChatDealCell  {
            let deal_cell = cell as! ChatDealCell
            let headers = collectionView.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader) as! [ChatContextHeader]
            if let header = headers.reversed().first(where: {$0.tag == indexPath.section }) {
                DispatchQueue.main.async {
                    header.product_onlinePrice.text = deal_cell.product_onlinePrice.text
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let layout = collectionViewLayout as? UICollectionViewFlowLayout // casting is required because UICollectionViewLayout doesn't offer header pin. Its feature of UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        return UIEdgeInsetsMake(17.0, 0, 20.0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(sectionatedMessages[section].count)
        return sectionatedMessages[section].count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        print(sectionatedMessages.count)
        return sectionatedMessages.count
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.loadingMessages || self.allMessagesLoaded {
            return
        }
        let offsetY = scrollView.contentOffset.y
        if offsetY <= 20 {
            var from:Int
            if let sent = messages.first?.sent {
                from = sent
            }
            else {
                from = Date().millisecondsTimeIntervalSince1970()
            }
            self.loadingMessages = true
            loadMoreMessages(from)
        }
    }
}
