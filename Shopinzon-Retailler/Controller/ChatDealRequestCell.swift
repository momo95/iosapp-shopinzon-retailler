//
//  ChatDealRequestCell.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 29/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
class ChatDealRequestCell:UICollectionViewCell {
    
    //MARK: Outlet
    @IBOutlet weak var productTitle_label: UILabel!
    @IBOutlet weak var productPrice_label: UILabel!
    @IBOutlet weak var productSize_label: UILabel!
    @IBOutlet weak var productEmbassador: UIImageView!
    
    var deal:DealRequest?
    func fill(message:DealRequest){
        self.deal = message
        if let context = message.context as? ProductContext {
            Server.getProduct(product_id: context.productId, completion: { (product) in
                DispatchQueue.main.async {
                    self.productTitle_label.text = "\(product!.name)\n\(product!.brand)"
                    if let size = context.size {
                        self.productSize_label.text = "Taille: \(size)"
                    }
                    else {
                        self.productSize_label.text = ""
                    }
                    self.productEmbassador.af_setImage(withURL: URL(string:product!.embassador!)!)
                    if let price = product?.shopPrice {
                        self.productPrice_label.text = "\(price)€"
                    }
                }
            })
        }
    }
    override func prepareForReuse() {
        DispatchQueue.main.async{
            self.productEmbassador.image = nil
        }
    }
    
    @IBAction func proposeDeal(_ sender: UIButton) {
        appDelegate.sendDeal(from: deal!)
        
    }
    @IBAction func rejectDeal(_ sender: UIButton) {
        appDelegate.rejectDeal(from: deal!)
    }
}
