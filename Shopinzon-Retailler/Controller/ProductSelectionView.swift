//
//  ProductSelectionView.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 31/10/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
protocol SelectionProduct {
    func selectedProduct(product_id:UInt64)
}
class ProductSelectionView : UIViewController {
    //MARK: Properties
    var shop:Shop {
        get {
            return MyShop.shop!
        }
    }
    var products:[Product] {
        get {
        return MyShop.catalog
        }
    }
        
    var loadedCatalogProducts:[Product] = []
    
    let product_loading_queue = DispatchQueue(label: "product_loading_queue.response", qos: .utility, attributes: [.concurrent])
    let titleLabel = UILabel()
    
    var productView:SelectionProduct!

    //MARK: Outlet
    @IBOutlet weak var catalog_products_collection: UICollectionView!
    
    //MARK: View Controller overriding
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCatalog()
    }
    override func loadView() {
        super.loadView()
        if let layout = catalog_products_collection.collectionViewLayout as? MosaicLayout {
            layout.delegate = self
        }
    }
    //MARK: Method
    func loadCatalog(){
        for product in  products.filter({$0.embassador != nil}){
            getImageAsync(imageUrl: product.embassador!){
                image in
                DispatchQueue.main.async {
                    product.picture = image
                    if self.loadedCatalogProducts.count != 0 {
                        self.catalog_products_collection.performBatchUpdates({
                            self.loadedCatalogProducts.append(product)
                            let indexPath = IndexPath(item: self.loadedCatalogProducts.count - 1 , section: 0)
                            self.catalog_products_collection.insertItems(at: [indexPath])
                            
                            self.catalog_products_collection.reloadData()
                            
                        }, completion: nil)
                        
                    }
                    else{
                        self.loadedCatalogProducts.append(product)
                        self.catalog_products_collection.reloadData()
                    }
                }
            }
        }
    }
    //MARK: Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "ShowProduct" {
            // Figure out which row was just tapped
            if let indexPath = catalog_products_collection.indexPathsForSelectedItems?.first {
                let selectedProduct:Product = loadedCatalogProducts[indexPath.row]
                // Get the item associated with this row and pass it along
                
            }
        }
    }
    
}
extension ProductSelectionView : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 1, animations: {
            cell.alpha = 1
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return loadedCatalogProducts.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = catalog_products_collection.dequeueReusableCell(withReuseIdentifier: "ShopCatalogViewCell", for: indexPath) as? ShopCatalogViewCell  else {
            fatalError("The dequeued cell is not an instance of ProductPresentationViewCell.")
        }
        
        let item = loadedCatalogProducts[indexPath.row]
        print(item.name)
        cell.product_title.text = item.name + " - " + item.brand.capitalized
        cell.product_title.sizeToFit()
        if let price = item.price?.value {
            cell.product_price.text = "\(price.format(f: ".2")) €"
        }
        else {
            cell.product_price.text = ""
        }
        cell.product_price.sizeToFit()
        if let picture = item.picture {
            cell.product_embassador.image = picture
            cell.alpha = 0
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        productView.selectedProduct(product_id: loadedCatalogProducts[indexPath.row].id)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension ProductSelectionView : MosaicLayoutDelegate {
    // 1. Returns the photo height
    func collectionView(collectionView:UICollectionView, heightForCellAtIndexPath indexPath:NSIndexPath , withWidth width:CGFloat) -> CGFloat {
        if let picture = loadedCatalogProducts[indexPath.item].picture{
            let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
            let rect  = AVMakeRect(aspectRatio: picture.size, insideRect: boundingRect)
            return rect.size.height
        }
        return 0
    }
    func changeHeight(height:CGFloat){
    }
    func cellPadingSize()->CGFloat{
        return 0
    }
}
