//
//  MyClientsViewController.swift
//  Shopinzon-Retailler
//
//  Created by Macho Uzan on 17/12/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
class MyClientsViewController:UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var clients:[Views.Client] = []
    var allLoaded = false
    var searchBar = UISearchBar()
    let titleLabel = UILabel()
    var filtredLevels_dic:[Int:Bool]{
        return [0:levelZero_checkbox.isSelected,
                1:levelOne_checkbox.isSelected,
                2:levelTwo_checkbox.isSelected,
                3:levelThree_checkbox.isSelected]
    }
    var filterLevel_list:[Int]?{
        let onlyTrue_dic = filtredLevels_dic.filter({$0.value == true})
        let onlyTrue_list = onlyTrue_dic.map({$0.key})
        if onlyTrue_list.count == 0 {
            return nil
        }
        return onlyTrue_list
    }
    @IBOutlet var rounded_views: [UIView]!
    @IBOutlet var checkbox_buttons: [UIButton]!

    
    @IBOutlet weak var filter_view: UIView!
    @IBOutlet weak var levelZero_checkbox: UIButton!
    @IBOutlet weak var levelOne_checkbox: UIButton!
    @IBOutlet weak var levelTwo_checkbox: UIButton!
    @IBOutlet weak var levelThree_checkbox: UIButton!

    
    @IBOutlet weak var clients_tableView: UITableView!
    @IBOutlet weak var indicator:UIActivityIndicatorView!
    
    //Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor =  UIColor(red: 35/255, green: 187/255, blue: 165/255, alpha: 1.0)
        let str = "Actualisation..."
        let myMutableString = NSMutableAttributedString(string: str)
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 15.0)!, range: NSRange(location:0,length:str.characters.count))
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 35/255, green: 187/255, blue: 165/255, alpha: 1.0), range: NSRange(location:0,length:str.characters.count))
        refreshControl.attributedTitle = myMutableString
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.default
        searchBar.showsCancelButton = true
        let myString:String = "Vos clients"
        let myMutableString = NSMutableAttributedString(string: myString)
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 18.0)!, range: NSRange(location:0,length:myString.characters.count))
        titleLabel.attributedText = myMutableString
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
        buildRightBarSearch()
        //Refresh
        if #available(iOS 10.0, *) {
            clients_tableView.refreshControl = refreshControl
        } else {
            clients_tableView.addSubview(refreshControl)
        }
        rounded_views.forEach({roundView($0)})
        checkbox_buttons.forEach({buttonToCheckbox($0)})
        hideSearchBar()
    }
    override func viewWillAppear(_ animated: Bool) {
       refresh()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClientTableViewCell", for: indexPath) as? ClientTableViewCell else {
            fatalError("The dequeued cell is not an instance of ClientTableViewCell.")
        }
        let item = clients[indexPath.row]
        cell.fill(clientView: item)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "ShowClient", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if allLoaded {
            return
        }
        if indexPath.row + 3 == clients.count{
            let last = clients.last!.lastPurchaseDate
            print(last)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            self.clients_tableView.tableFooterView = spinner
            self.clients_tableView.tableFooterView?.isHidden = false
            Server.getClientList(from: clients.last!.user.email,completion: { (clients) in
                if clients.count == 0 {
                    self.allLoaded = true
                }
                self.clients.append(contentsOf: clients)
                DispatchQueue.main.async {
                    self.clients_tableView.reloadData()
                    self.indicator.stopAnimating()
                    spinner.stopAnimating()
                    self.clients_tableView.tableFooterView?.isHidden = true
                }
            })
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "ShowClient",
            let selection = self.clients_tableView.indexPathForSelectedRow {
            let clientViewController = segue.destination as! ClientViewController
            clientViewController.clientView = clients[selection.row]
        }
    }
    
}
extension MyClientsViewController:UISearchBarDelegate{
    func showSearchBar() {
        searchBar.alpha = 0
        filter_view.isHidden = true
        navigationItem.titleView = searchBar
        DispatchQueue.main.async {
            self.navigationItem.setRightBarButton(nil, animated: true)
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.filter_view.isHidden = false
            self.searchBar.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
            
        })
    }
    
    func hideSearchBar() {
        self.buildRightBarSearch()
        self.titleLabel.alpha = 0
        navigationItem.titleView = titleLabel
        filter_view.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 1
            self.filter_view.isHidden = true
        }, completion: { finished in
            
        })
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        Server.getClientList(query: searchText,levels:filterLevel_list) { (clients) in
            self.clients = clients
            DispatchQueue.main.async {
                self.clients_tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }

    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        hideSearchBar()
        DispatchQueue.main.async {
            self.clients = []
            self.clients_tableView.reloadData()
        }
        Server.getClientList() { (clients) in
            self.clients = clients
            DispatchQueue.main.async {
                self.clients_tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    @IBAction func searchButtonPressed(sender: AnyObject) {
        showSearchBar()
        refresh()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let text = searchBar.text {
            DispatchQueue.main.async {
                self.clients = []
                self.clients_tableView.reloadData()
            }
            self.indicator.startAnimating()
            Server.getClientList(query: text){ (clients) in
                self.clients = clients
                DispatchQueue.main.async {
                    self.clients_tableView.reloadData()
                    self.indicator.stopAnimating()
                }
            }
        }
    }
    func buildRightBarSearch(){
        let searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.searchButtonPressed(sender:)))
        navigationItem.setRightBarButton(searchBarButtonItem, animated: true)
        searchBarButtonItem.image = svg2Image(svgName: "search", size: CGSize(width:25,height:25) , color: UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1.0))
    }
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        DispatchQueue.main.async {
            self.clients = []
            self.clients_tableView.reloadData()
        }
        refresh()
    }
    func refresh(){
        let text = searchBar.alpha == 0 ? nil : searchBar.text
        let filterLevel = searchBar.alpha == 0 ? nil : filterLevel_list
        Server.getClientList(query: text,levels:filterLevel) { (clients) in
            self.clients = clients
            DispatchQueue.main.async {
                self.clients_tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    @IBAction func checkbox_click(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        Server.getClientList(query: searchBar.text,levels:filterLevel_list) { (clients) in
            self.clients = clients
            DispatchQueue.main.async {
                self.clients_tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
}

