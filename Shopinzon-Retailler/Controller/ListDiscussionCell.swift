//
//  ListDiscussionCell.swift
//  Shopinzon
//
//  Created by Macho Uzan on 24/05/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit

class ListDiscussionCell: UITableViewCell {
    //MARK: Outlet
    @IBOutlet weak var entity_logo: UIImageView!
    
    @IBOutlet weak var last_message_text_view: UILabel!
    
    @IBOutlet weak var last_message_date_label: UILabel!
    
    @IBOutlet weak var entity_name_label: UILabel!

}

