//
//  MasaicLayout.swift
//  Shopinzon
//
//  Created by Macho Uzan on 26/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
protocol MosaicLayoutDelegate {
    func collectionView(collectionView:UICollectionView, heightForCellAtIndexPath indexPath:NSIndexPath,
                        withWidth:CGFloat) -> CGFloat
    func changeHeight(height:CGFloat)
    func cellPadingSize() -> CGFloat
}
class MosaicLayout:UICollectionViewLayout {
    var delegate: MosaicLayoutDelegate!
    
    var numberOfColumns = 2
    var cellPadding: CGFloat = 0.0
    
    private var cache = [UICollectionViewLayoutAttributes]()
    
    private var contentHeight: CGFloat  = 0.0
    private var contentWidth: CGFloat {
        let insets = collectionView!.contentInset
        return collectionView!.bounds.width - (insets.left + insets.right)
    }
    var columnWidth: CGFloat {
        return contentWidth / CGFloat(numberOfColumns)
    }
    
    var xOffset:[CGFloat] = []
    var yOffset:[CGFloat] = []
    
    override func prepare() {
        // 1
        if cache.isEmpty {
            // 2
            for column in 0 ..< numberOfColumns {
                xOffset.append(CGFloat(column) * columnWidth )
            }
            cellPadding = delegate.cellPadingSize()
            yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
            
            for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
                let minColumn = yOffset.min()!
                let minColumnIndex = yOffset.index(of: minColumn)!
                let indexPath = NSIndexPath(item: item, section: 0)
                
                let width = columnWidth - cellPadding * 2
                let cellHeight = delegate.collectionView(collectionView:collectionView!, heightForCellAtIndexPath: indexPath,
                                                          withWidth:width)
                let height = cellPadding +  cellHeight + cellPadding
                
                
                let frame = CGRect(x: xOffset[minColumnIndex], y: yOffset[minColumnIndex], width: columnWidth, height: height)
                let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                yOffset[minColumnIndex] = yOffset[minColumnIndex] + height + cellPadding

                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath as IndexPath)
                attributes.frame = insetFrame
                cache.append(attributes)
                contentHeight = max(contentHeight, frame.maxY)
                delegate.changeHeight(height:contentHeight)
                self.invalidateLayout()
            }
        }
    }
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        objc_sync_enter(self)
        if cache.count < indexPath.row + 1 {
            let minColumn = yOffset.min()!
            let minColumnIndex = yOffset.index(of: minColumn)!            
            let width = columnWidth - cellPadding * 2
            let cellHeight = delegate.collectionView(collectionView:collectionView!, heightForCellAtIndexPath: indexPath as NSIndexPath,
                                                      withWidth:width)
            let height = cellPadding +  cellHeight  + cellPadding
            
            
            let frame = CGRect(x: xOffset[minColumnIndex], y: yOffset[minColumnIndex], width: columnWidth, height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            yOffset[minColumnIndex] = yOffset[minColumnIndex] + height + cellPadding
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath as IndexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            contentHeight = max(contentHeight, frame.maxY)
            delegate.changeHeight(height:contentHeight)
        }
        objc_sync_exit(self)
        return cache[indexPath.row]
    }
}
