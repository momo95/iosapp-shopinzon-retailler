//
//  InitViewController.swift
//  Shopinzon
//
//  Created by Macho Uzan on 15/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
import Firebase

class InitViewController: UIViewController {
    //MARK: Properties
    
    //MARK: Outlet
    
    //MARK: View Controller overriding
    override func viewDidLoad() {
        super.viewDidLoad()
        if Auth.auth().currentUser == nil {
            performSegue(withIdentifier: "NotConnected", sender: nil)
        }
        else {
            DispatchQueue.main.async {
                let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                activityView.center = self.view.center
                activityView.startAnimating()
                self.view.addSubview(activityView)
            }
            appDelegate.initDiscussions(then: {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "Connected", sender: nil)
                }
            })
        }
    }
    
    
    
    //MARK: Method
    
    //MARK: Action
    
    //MARK: Segue Method
    
}

