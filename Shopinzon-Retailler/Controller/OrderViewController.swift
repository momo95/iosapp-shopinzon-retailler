//
//  OrderViewController.swift
//  Shopinzon
//
//  Created by Macho Uzan on 01/11/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import UIKit
class OrderViewController : UIViewController{
    var orderView:(order:Order,product:Product,user:Order.User)!
    var order:Order {
        return orderView.order
    }
    var product:Product {
        return orderView.product
    }
    var user:Order.User {
        return orderView.user
    }
    //Order Info
    @IBOutlet weak var order_number: UILabel!
    @IBOutlet weak var order_date: UILabel!
    
    //User
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var user_family: UILabel!
    @IBOutlet weak var user_mail: UILabel!
    @IBOutlet weak var user_phone: UILabel!
    @IBOutlet weak var user_address: UILabel!
    @IBOutlet weak var user_zipCode: UILabel!
    @IBOutlet weak var user_city: UILabel!
    
    //Shop
    @IBOutlet weak var shop_logo: UIImageView!
    @IBOutlet weak var shop_name: UILabel!
    @IBOutlet weak var shop_description: UILabel!
    
    //Product
    @IBOutlet weak var product_embassador: UIImageView!
    @IBOutlet weak var product_description: UILabel!
    @IBOutlet weak var product_price: UILabel!
    @IBOutlet weak var product_size: UILabel!
    
    //Payement Mode
    @IBOutlet weak var payement_mode: UILabel!
    @IBOutlet weak var payementMode_details: UILabel!

    //Info sup
    @IBOutlet weak var info_sup: UILabel!

    //State
    @IBOutlet weak var state_textField: UITextField!

        //MARK: View Controller overriding
    override func viewDidLoad(){
        super.viewDidLoad()

        user_name.text = order.user.FirstName
        user_family.text = order.user.LastName
        user_mail.text = order.user.email
        user_phone.text = ""
        user_address.text = order.user.invoiceAddress.formated_address
        user_zipCode.text = order.user.invoiceAddress.postal_code
        user_city.text = order.user.invoiceAddress.locality
    

        payement_mode.text = "En carte bleue \(order.payment)"
        payementMode_details.text = ""
        
        info_sup.text = ""
        order_number.text = "Facture n°\(order.id)"
        let date = Date(timeIntervalSince1970: TimeInterval(order.time/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/yyyy")
        order_date.text = dateFormatter.string(from: date )
        
        
        
        product_embassador.af_setImage(withURL: URL(string: product.embassador!)!)
        product_description.text = "\(product.name)\n\(product.brand)"
        product_price.text =  "\(order.paid)€"
        product_size.text = "Taille : \(order.size)"
        
        state_textField.backgroundColor = tealishColor
        
        let string = "Statut : \(order.state)"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Light", size: 14.0)!, range: NSRange(location: 0, length: 8))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!, range: NSRange(location: 9, length: string.characters.count - 9))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:0,length:string.characters.count))
        state_textField.attributedText = attributedString
        //total_label.text =  "\(order.paid)€"
    }
}
