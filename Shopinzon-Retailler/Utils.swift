//
//  AsyncImageLoading.swift
//  Shopinzon
//
//  Created by Macho Uzan on 29/03/2017.
//  Copyright © 2017 Macho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON
import SVGKit
import MapKit
import AVFoundation
let screenHeight = UIScreen.main.bounds.size.height
let screenWidth = UIScreen.main.bounds.size.width
let image_loading_queue = DispatchQueue(label: "image_loading_queue.response", qos: .utility, attributes: [.concurrent])
let downloader = ImageDownloader(
    configuration: ImageDownloader.defaultURLSessionConfiguration(),
    downloadPrioritization: .lifo,
    maximumActiveDownloads: 10,
    imageCache: AutoPurgingImageCache()
)

func isInInterval(array:[Int],value:Int)->Bool{
    for i in stride(from: 0, to: array.count, by: 2){
        let open = array[i]
        let close = array[i+1]
        if (value >= open && value <= close){
            return true
        }
    }
    return false
}
func dayOfWeekFor(minutes:Int)->Int{
    switch minutes {
    case 0...1439 :
        return 0
    case 1440...2879 :
        return 1
    case 2880...4319 :
        return 2
    case 4320...5759 :
        return 3
    case 5760...7199 :
        return 4
    case 7200...8639 :
        return 5
    case 8640...10079 :
        return 6
    default:
        return -1
    }
}
func minutesFromDayBegin(minutesFromWeekBegin:Int)->Int{
    let day = dayOfWeekFor(minutes:minutesFromWeekBegin)
    let minutes = minutesFromWeekBegin - (day * 24 * 60)
    return minutes
}
func minutesFromWeekBegin(day:Int,minutesFromDayBegin:Int)->Int{
    let minutes = minutesFromDayBegin + (day * 24 * 60)
    return minutes
}
func getImageAsync(imageUrl:String,threadQueue:DispatchQueue = image_loading_queue,completion: @escaping (_ image: UIImage) -> Void)
{
    let urlRequest = URLRequest(url: URL(string: imageUrl)!)
    downloader.download(urlRequest) { response in
        switch response.result {
        case .success(let image):
            image.af_inflate()
            completion(image)
        case .failure(let error):
            print("Request failed with error: \(error)")
        }
        return
    }
}


func productOnImageView(product:Product,imageView:UIImageView,indicator:UIActivityIndicatorView,threadQueue:DispatchQueue)
{
    getImageAsync(imageUrl: product.embassador!){ image in
        DispatchQueue.main.sync{
            indicator.removeFromSuperview()
            imageView.image = image
            product.picture = image
        }
    }
}
func userlogoOnImageView(user:User,imageView:UIImageView,threadQueue:DispatchQueue)
{
    let indicator:UIActivityIndicatorView  = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    indicator.startAnimating()
    indicator.center = imageView.center
    imageView.addSubview(indicator)
    /*getImageAsync(imageUrl: user.logo.url){ image in
        DispatchQueue.main.sync{
            indicator.removeFromSuperview()
            imageView.image = image
            user.logoPicture = image
        }

    }*/
}


func getJSONSync(urlToRequest: String) -> NSData?{
    do
    {
        return try NSData(contentsOf: NSURL(string: urlToRequest) as! URL)
    }
    catch
    {
        return nil
    }
}
func svg2Image(svgName:String,size:CGSize,color:UIColor)->UIImage{
    let imageData = NSDataAsset(name: svgName)
    let SVGImage: SVGKImage = SVGKImage(data: imageData?.data)
    SVGImage.scaleToFit(inside: size)
    SVGImage.setTintColor(color: color)
    return SVGImage.uiImage
}
func svgOnImageView(svgName:String,imageView:UIImageView,color:UIColor){
    imageView.image = svg2Image(svgName:svgName,size:imageView.frame.size,color:color)
}
func getPictureHeight(picture:UIImage?,width:CGFloat) -> CGFloat{
    if picture == nil {
        return width
    }
    else{
        let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
        let rect  = AVMakeRect(aspectRatio: picture!.size, insideRect: boundingRect)
        return rect.size.height
    }
}
func getPictureWidth(picture:UIImage?,height:CGFloat) -> CGFloat{
    if picture == nil {
        return height
    }
    else{
        let boundingRect =  CGRect(x: 0, y: 0, width: CGFloat(MAXFLOAT), height: height)
        let rect  = AVMakeRect(aspectRatio: picture!.size, insideRect: boundingRect)
        return rect.size.width
    }
}
func centerMapOnLocation(location: CLLocation,map:MKMapView) {
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,500, 500)
    map.setRegion(coordinateRegion, animated: true)
}
